package com.herr.springboot.http.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpRestTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(HttpRestTemplateApplication.class, args);
    }
}
