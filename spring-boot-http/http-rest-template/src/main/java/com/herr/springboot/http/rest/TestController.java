package com.herr.springboot.http.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author herr05
 */
@RestController
public class TestController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/rest")
    public String rest() {

        ResponseEntity<String> responseEntity = restTemplate.getForEntity("https://www.baidu.com/sugrec?prod=pc_his&from=pc_web", String.class);
        String body = responseEntity.getBody();
        return body;
    }
}
