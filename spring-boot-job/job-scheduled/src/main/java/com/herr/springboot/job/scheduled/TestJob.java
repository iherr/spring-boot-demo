package com.herr.springboot.job.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TestJob {
    private static final Logger logger = LoggerFactory.getLogger(TestJob.class);

    /**
     * 检查1
     */
    @Scheduled(cron = "0 30 12 * * ?")
    public void checkState1() {
        logger.info("12:30开始....");
        logger.info("12:30完成....");
    }

    /**
     * 检查2
     */
    @Scheduled(cron = "0 30 18 * * ?")
    public void checkState2() {
        logger.info("18:30开始....");
        logger.info("18:30完成....");
    }
}
