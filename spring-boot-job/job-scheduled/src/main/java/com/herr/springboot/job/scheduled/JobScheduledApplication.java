package com.herr.springboot.job.scheduled;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobScheduledApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobScheduledApplication.class, args);
    }
}
