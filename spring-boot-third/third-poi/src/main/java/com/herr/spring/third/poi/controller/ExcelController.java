package com.herr.spring.third.poi.controller;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author herr05
 */
@RequestMapping("/excel")
@RestController
public class ExcelController {

    @RequestMapping("/get")
    public String get() throws IOException {
        Workbook wb=new HSSFWorkbook();
        Sheet sheet = wb.createSheet("sheet页");
        Row row=sheet.createRow(1);

        //创建一个字体
        //Font font=wb.createFont();
        //Font font=wb.getFontAt((short) 0);
        //font.setFontHeightInPoints((short) 24);
        //font.setFontName("Courier New ");
        //font.setItalic(true);
        //font.setStrikeout(true);

        CellStyle style=wb.createCellStyle();
        //style.setFont(font);

        Cell cell = row.createCell((short) 1);
        cell.setCellValue("你好呀");
        cell.setCellStyle(style);

        FileOutputStream is=new FileOutputStream("D:\\1.xls");
        //FileOutputStream is=new FileOutputStream("/home/appadmin/mdr/test/1.xls");
        wb.write(is);
        is.close();
        return "success";
    }

}
