package com.herr.spring.third.poi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */
@SpringBootApplication
public class ThirdPoiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ThirdPoiApplication.class,args);
    }
}
