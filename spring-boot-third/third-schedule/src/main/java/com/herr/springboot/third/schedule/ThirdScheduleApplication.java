package com.herr.springboot.third.schedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ThirdScheduleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThirdScheduleApplication.class, args);
    }

}
