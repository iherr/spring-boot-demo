package com.herr.springboot.third.schedule;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
public class HelloService {

    @Scheduled(fixedDelay = 2000)//前面任务的结束时间和后面任务的开始时间之间间隔2s
    public void  fixedDelay() throws InterruptedException {
        System.out.println("fixedDelay begin>>"+new Date());
        TimeUnit.SECONDS.sleep(1);
        System.out.println("fixedDelay end>>"+new Date());
    }

    @Scheduled(fixedRate = 2000)//两次定时任务开始的间隔时间为2s
    public void fixedRate() {
        System.out.println("fixedRate>>"+new Date());
    }

    @Scheduled(cron = "0/5 17 * * * ?")
    public void cron() {
        System.out.println("cron>>"+new Date());
    }
}
