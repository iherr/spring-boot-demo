package com.herr.springboot.third.mail.service;

import jakarta.mail.MessagingException;

/**
 * @author herr05
 */
public interface MailService {
    void sendMail(String to, String subject, String content);

    void sendMail2(String to, String subject, String content) throws MessagingException;

    void sendMail3(String to, String subject) throws MessagingException;
}
