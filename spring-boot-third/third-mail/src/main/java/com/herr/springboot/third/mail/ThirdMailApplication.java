package com.herr.springboot.third.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */

@SpringBootApplication
public class ThirdMailApplication {
    public static void main(String[] args) {
        SpringApplication.run(ThirdMailApplication.class,args);
    }
}
