package com.herr.springboot.third.mail.service.impl;

import com.herr.springboot.third.mail.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import java.io.File;

/**
 * @author herr05
 */
@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String sender;

    @Async
    @Override
    public void sendMail(String to, String subject, String content) {
        SimpleMailMessage mailMessage=new SimpleMailMessage();
        mailMessage.setFrom(sender);
        mailMessage.setTo(to);
        mailMessage.setSubject(subject);
        mailMessage.setText(content);
        try {
            javaMailSender.send(mailMessage);
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("发送邮件失败");
        }
    }

    @Async
    @Override
    public void sendMail2(String to, String subject, String content) throws MessagingException {
        MimeMessage msg= javaMailSender.createMimeMessage();
        MimeMessageHelper helper= new MimeMessageHelper(msg, true);
        helper.setFrom(sender);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(content);
        helper.addInline("p01", new FileSystemResource(new File("D:\\1.png")));
        helper.addAttachment("1.png", new File("D:\\1.png"));

        try {
            javaMailSender.send(msg);
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("发送邮件失败");
        }
    }

    @Autowired
    TemplateEngine templateEngine;

    @Async
    @Override
    public void sendMail3(String to, String subject) throws MessagingException {
        MimeMessage msg= javaMailSender.createMimeMessage();
        MimeMessageHelper helper= new MimeMessageHelper(msg, true);
        helper.setFrom(sender);
        helper.setTo(to);
        helper.setSubject(subject);

        Context context = new Context();
        context.setVariable("name", "herr");
        context.setVariable("age", "18");
        context.setVariable("addr", "bj");
        String process = templateEngine.process("mail.html", context);
        helper.setText(process, true);

        try {
            javaMailSender.send(msg);
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("发送邮件失败");
        }
    }
}
