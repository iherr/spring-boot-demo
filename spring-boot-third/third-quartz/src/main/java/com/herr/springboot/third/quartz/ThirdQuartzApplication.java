package com.herr.springboot.third.quartz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ThirdQuartzApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThirdQuartzApplication.class, args);
    }

}
