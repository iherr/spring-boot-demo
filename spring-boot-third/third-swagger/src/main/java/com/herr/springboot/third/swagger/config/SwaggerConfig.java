package com.herr.springboot.third.swagger.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Swagger文档配置和初始化
 *
 * @author herr05
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${application.swagger.showui:true}")
    private boolean showui;

    @Bean
    public Docket api() {
        if (showui) {
            return new Docket(DocumentationType.SWAGGER_2)
                .globalOperationParameters(getParameters())
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                //.paths(Predicates.or(
                //        PathSelectors.ant("/*/**")
                //))
                .build()
                .groupName("分组1")
                .useDefaultResponseMessages(false)
                .ignoredParameterTypes();
        }else {
            return new Docket(DocumentationType.SWAGGER_2)
                    .apiInfo(apiInfo())
                    .select()
                    .paths(PathSelectors.none()).build().groupName("分组1").useDefaultResponseMessages(false);
        }
    }

    /**
     * 获取swagger ApiInfo
     *
     * @return
     */
    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("API文档")
                .termsOfServiceUrl("")
                .version("1.0")
                .build();
    }

    /**
     * 获取Swagger参数
     *
     * @return
     */
    List<Parameter> getParameters() {
        return new ArrayList<Parameter>(Arrays.asList(
                new ParameterBuilder()
                        .name("uid")
                        .description("用户id")
                        .modelRef(new ModelRef("string"))
                        .parameterType("header")
                        .required(true)
                        .defaultValue("1")
                        .build(),
                new ParameterBuilder()
                        .name("token")
                        .description("鉴权信息")
                        .modelRef(new ModelRef("string"))
                        .parameterType("header")
                        .required(true)
                        .defaultValue("test")
                        .build()
        ));
    }

}
