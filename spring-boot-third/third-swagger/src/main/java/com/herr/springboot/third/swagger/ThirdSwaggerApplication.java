package com.herr.springboot.third.swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */

@SpringBootApplication
public class ThirdSwaggerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ThirdSwaggerApplication.class,args);
    }
}
