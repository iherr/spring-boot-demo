package com.herr.springboot.third.swagger.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author herr05
 */
//@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Value("${application.swagger.showui:true}")
    private boolean showui;

    /**
     * 静态资源映射器
     * @param registry
     */
    @Override
    public  void addResourceHandlers(ResourceHandlerRegistry registry) {

        // 由于spring.resources.add-mappings为false，spring会关闭自动映射。如需要使用静态资源，在此添加映射
        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/");
        // 添加swagger静态资源映射，如是生产环境，自动不添加。
        if (showui) {
            registry.addResourceHandler("swagger-ui.html")
                    .addResourceLocations("classpath:/META-INF/resources/");
            registry.addResourceHandler("/webjars/**")
                    .addResourceLocations("classpath:/META-INF/resources/webjars/");
        }
    }
}
