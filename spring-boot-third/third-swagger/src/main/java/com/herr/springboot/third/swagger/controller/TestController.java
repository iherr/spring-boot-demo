package com.herr.springboot.third.swagger.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */

@RestController
public class TestController {
    @ApiOperation("test接口")
    @GetMapping("/test")
    public String test(){
        return "test";
    }
}
