package com.herr.springboot.data.mybatismulti.controller;

import com.herr.springboot.data.mybatismulti.model.User;
import com.herr.springboot.data.mybatismulti.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    //查询primary库
    @RequestMapping("/selectById")
    public User selectById(Integer id){
        return userService.selectById(id);
    }

    //查询secondary库
    @RequestMapping("/selectById2")
    public User selectById2(Integer id){
        return userService.selectById2(id);
    }

}
