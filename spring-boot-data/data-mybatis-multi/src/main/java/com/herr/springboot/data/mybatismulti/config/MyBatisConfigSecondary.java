package com.herr.springboot.data.mybatismulti.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import jakarta.annotation.Resource;
import javax.sql.DataSource;

/**
 * Primary数据源配置
 */
@Configuration
@MapperScan(basePackages = MyBatisConfigSecondary.PACKAGE,sqlSessionFactoryRef = "secondarySqlSessionFactory")
public class MyBatisConfigSecondary {

    @Value("${spring.datasource.secondary.url}")
    private String url;

    @Value("${spring.datasource.secondary.username}")
    private String user;

    @Value("${spring.datasource.secondary.password}")
    private String password;

    @Value("${spring.datasource.secondary.driver-class-name}")
    private String driverClass;

    // 数据源mapper隔离
    static final String PACKAGE = "com.herr.springboot.data.mybatismulti.mapper.secondary";
    static final String MAPPER_LOCATION = "classpath:mapping/secondary/*Mapper.xml";

    @Bean(name = "dsSecondary")
    @ConfigurationProperties(prefix = "spring.datasource.secondary")
    DataSource dsSecondary() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driverClass);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        //可以添加其他配置参数
        return dataSource;
    }


    @Bean(name = "secondarySqlSessionFactory")
    SqlSessionFactory secondarySqlSessionFactory(@Qualifier("dsSecondary") DataSource dsSecondary) {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        try {
            bean.setDataSource(dsSecondary);
            bean.setMapperLocations(new PathMatchingResourcePatternResolver().
                    getResources(MyBatisConfigSecondary.MAPPER_LOCATION));
            return bean.getObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //@Bean
    //SqlSessionTemplate sqlSessionTemplateSecondary(@Qualifier("secondarySqlSessionFactory") SqlSessionFactory secondarySqlSessionFactory) {
    //    return new SqlSessionTemplate(secondarySqlSessionFactory);
    //}

    @Bean(name = "transactionManagerSecondary")
    public DataSourceTransactionManager transactionManagerSecondary(@Qualifier("dsSecondary") DataSource dsSecondary) {
        return new DataSourceTransactionManager(dsSecondary);
    }
}
