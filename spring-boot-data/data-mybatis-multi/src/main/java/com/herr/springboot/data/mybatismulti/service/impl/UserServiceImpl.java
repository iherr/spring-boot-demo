package com.herr.springboot.data.mybatismulti.service.impl;

import com.herr.springboot.data.mybatismulti.mapper.primary.UserMapper;
import com.herr.springboot.data.mybatismulti.mapper.secondary.UserMapper2;
import com.herr.springboot.data.mybatismulti.model.User;
import com.herr.springboot.data.mybatismulti.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserMapper2 userMapper2;

    @Override
    public User selectById(int primarykey) {
        User user = userMapper.selectById2(primarykey);
        return user;
    }

    @Override
    public User selectById2(int primarykey) {
        User user = userMapper2.selectById2(primarykey);
        return user;
    }

}
