package com.herr.springboot.data.mybatismulti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */
@SpringBootApplication
public class DataMybatisMultiApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataMybatisMultiApplication.class,args);
    }
}
