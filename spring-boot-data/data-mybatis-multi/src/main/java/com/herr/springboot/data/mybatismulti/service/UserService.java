package com.herr.springboot.data.mybatismulti.service;


import com.herr.springboot.data.mybatismulti.model.User;

public interface UserService {

    User selectById(int primarykey);

    User selectById2(int primarykey);

}
