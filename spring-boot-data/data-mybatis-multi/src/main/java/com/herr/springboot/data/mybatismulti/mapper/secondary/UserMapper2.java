package com.herr.springboot.data.mybatismulti.mapper.secondary;

import com.herr.springboot.data.mybatismulti.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.BaseMapper;

/**
 * @author herr05
 */
@Mapper
// 该注解用于规避IDEA mapper注入时warn显示could not autowire，无实际作用
@Component(value = "userMapper2")
public interface UserMapper2 extends BaseMapper<User> {

    User selectById(Integer userId);

    User selectById2(Integer userId);
    //
    //int insert(User user);
    //
    //List<User> selectUsers();

}