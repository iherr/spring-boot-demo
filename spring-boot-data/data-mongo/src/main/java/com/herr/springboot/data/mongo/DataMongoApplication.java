package com.herr.springboot.data.mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */
@SpringBootApplication
public class DataMongoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataMongoApplication.class,args);
    }
}
