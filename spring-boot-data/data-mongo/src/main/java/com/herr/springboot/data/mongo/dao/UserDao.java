package com.herr.springboot.data.mongo.dao;

import com.herr.springboot.data.mongo.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path="user")
public interface UserDao extends MongoRepository<User,String> {
    List<User> findUserByNameContaining(String name);
}
