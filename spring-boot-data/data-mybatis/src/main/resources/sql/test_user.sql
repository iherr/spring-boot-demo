create table user
(
    user_id   int auto_increment
        primary key,
    user_name varchar(10) null,
    age       varchar(3)  null,
    password  varchar(20) null,
    phone     varchar(20) null
);

INSERT INTO test.user (user_id, user_name, age, password, phone) VALUES (1, 'herr', '20', null, '2');