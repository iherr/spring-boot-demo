create table role
(
    id        int         null,
    role_name varchar(20) null
);

INSERT INTO test.role (id, role_name) VALUES (1, '管理员');
INSERT INTO test.role (id, role_name) VALUES (2, '一般人员');