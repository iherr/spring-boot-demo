create table user_role
(
    user_id int null,
    role_id int null
);

INSERT INTO test.user_role (user_id, role_id) VALUES (1, 1);
INSERT INTO test.user_role (user_id, role_id) VALUES (1, 2);
INSERT INTO test.user_role (user_id, role_id) VALUES (2, 1);