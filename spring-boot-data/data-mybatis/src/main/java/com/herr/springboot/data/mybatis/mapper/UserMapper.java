package com.herr.springboot.data.mybatis.mapper;

import com.herr.springboot.data.mybatis.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
// 该注解用于规避IDEA mapper注入时warn显示could not autowire，无实际作用
@Component(value = "userMapper")
public interface UserMapper {

    User selectById(long userId);

    User selectById2(long userId);

    User selectById3(long userId);

    @Select("select user_id as userId,user_name as userName,password,phone from user where user_name = #{name}")
    User findByName(String name);

    //在配置map-underscore-to-camel-case后，支持自动根据驼峰命名规则进行转换
    //可通过@Param注解变更注入的参数名
    @Select("select user_id,user_name,password,phone from user where user_name = #{username}")
    User findByName2(@Param("username") String name);

    // 也可使用注解进行字段映射，添加id可实现复用
    @Results(id = "userMap",value = {
            @Result(property = "userId",column = "user_id",id = true),
            @Result(property = "userName",column = "user_name")
    })
    @Select("select user_id,user_name,password,phone from user where user_name = #{name}")
    User findByName3(String name);

    // 复用该id的字段映射
    @ResultMap("userMap")
    @Select("select user_id,user_name,password,phone from user where user_name = #{name} and phone=#{phone} ")
    User findByNameAndPhone(String name,String phone);

    User findByNameAndPhone2(String name,String phone);

    User findByNameAndPhone3(String name,String phone);

    int deleteById(long userId);

    @Delete("delete from user where user_id = #{userId}")
    int deleteById2(long userId);

    int insert(User user);

    int insert2(User user);

    int insertMysql(User user);

    int insertOracle(User user);

    int insertSelective(User user);

    @Insert({"insert into user (user_id, user_name, password,phone)" ,
            "values (#{userId}, #{userName}, #{password}, #{phone})"})
    int insert3(User user);

    @Insert({"insert into user (user_id, user_name, password,phone)" ,
            "values (#{userId}, #{userName}, #{password}, #{phone})"})
    @Options(useGeneratedKeys = true, keyProperty = "userId",keyColumn ="user_id")
    int insert4(User user);

    @Insert({"insert into user (user_id, user_name, password,phone)" ,
            "values (#{userId}, #{userName}, #{password}, #{phone})"})
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "userId",keyColumn ="user_id",resultType = Long.class, before = false)
    int insertMysql2(User user);

    int updateById(User user);

    @Update({"update user",
            "set user_name = #{userName},",
            "password = #{password},",
            "phone = #{phone}",
            "where user_id = #{userId}"
    })
    int updateById2(User user);

    int updateByIdSelective(User user);

    List<User> selectUsers();

    @Select({"select u.user_id as userId,u.user_name as userName,u.password,u.phone",
            "from user u",
            "inner join user_role ur on u.user_id=ur.user_id",
            "where ur.role_id = #{role_id}"
    })
    List<User> selectUsersByRoleId(long role_id);

}