package com.herr.springboot.data.mybatis.controller;

import com.herr.springboot.data.mybatis.model.User;
import com.herr.springboot.data.mybatis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/selectById")
    public Object selectById(@RequestParam(name = "id", required = false) long userId){
        return userService.selectById(userId);
    }

    @GetMapping("/findByName")
    public Object findByName(String name){
        return userService.findByName(name);
    }

    @GetMapping("/deleteById")
    public Object deleteById(long userId){
        return userService.deleteById(userId);
    }

    @GetMapping("/findByNameAndPhone")
    public Object findByNameAndPhone(String name,String phone){
        return userService.findByNameAndPhone(name,phone);
    }

    @PostMapping("/insertUser")
    public int insertUser(User user){
        return userService.insert(user);
    }

    @PostMapping("/insertSelective")
    public int insertSelective(User user){
        return userService.insertSelective(user);
    }

    @PostMapping("/updateById")
    public int updateById(User user){
        return userService.updateById(user);
    }

    @PostMapping("/updateByIdSelective")
    public int updateByIdSelective(User user){
        return userService.updateByIdSelective(user);
    }

    @GetMapping("/selectUsers")
    public List<User> selectUsers(){
        return userService.selectUsers();
    }


}
