package com.herr.springboot.data.mybatis.mapper;

import com.herr.springboot.data.mybatis.model.Role;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author herr05
 */

@Mapper
@Component(value = "roleMapper")
public interface RoleMapper {

    Role selectById(long id);

    Role selectById2(long id);

    Role selectById3(long id);

    Role selectRoleById(long id);
}
