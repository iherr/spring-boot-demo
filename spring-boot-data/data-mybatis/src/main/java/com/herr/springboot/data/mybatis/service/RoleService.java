package com.herr.springboot.data.mybatis.service;

import com.herr.springboot.data.mybatis.model.Role;
import com.herr.springboot.data.mybatis.model.User;

/**
 * @author herr05
 */
public interface RoleService {

    Role selectById(long id);

    Role selectById2(long id);

    Role selectById3(long id);

    Role selectRoleById(long id);
}
