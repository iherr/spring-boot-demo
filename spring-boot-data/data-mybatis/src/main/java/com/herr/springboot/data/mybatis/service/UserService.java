package com.herr.springboot.data.mybatis.service;

//import com.github.pagehelper.PageInfo;
import com.herr.springboot.data.mybatis.model.User;

import java.util.List;

public interface UserService {

    User selectById(long userId);

    User findByName(String name);

    User findByNameAndPhone(String name,String phone);

    int deleteById(long userId);

    int insert(User user);

    int insertSelective(User user);

    int updateById(User user);

    int updateByIdSelective(User user);

    List<User> selectUsers();

}
