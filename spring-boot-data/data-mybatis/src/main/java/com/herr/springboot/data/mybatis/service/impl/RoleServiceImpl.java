package com.herr.springboot.data.mybatis.service.impl;

import com.herr.springboot.data.mybatis.mapper.RoleMapper;
import com.herr.springboot.data.mybatis.model.Role;
import com.herr.springboot.data.mybatis.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author herr05
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public Role selectById(long id) {
        return roleMapper.selectById(id);
    }

    @Override
    public Role selectById2(long id) {
        return roleMapper.selectById2(id);
    }

    @Override
    public Role selectById3(long id) {
        return roleMapper.selectById3(id);
    }

    @Override
    public Role selectRoleById(long id) {
        return roleMapper.selectRoleById(id);
    }
}
