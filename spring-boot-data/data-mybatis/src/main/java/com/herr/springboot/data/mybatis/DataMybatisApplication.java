package com.herr.springboot.data.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//每个Mapper接口都要加上@Mapper注解，比较麻烦，解决这个问题用@MapperScan
//@MapperScan("com.herr.springboot.data.mybatis.mapper")
public class DataMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataMybatisApplication.class, args);
    }
}
