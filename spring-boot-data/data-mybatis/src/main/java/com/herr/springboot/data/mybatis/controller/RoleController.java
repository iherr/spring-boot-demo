package com.herr.springboot.data.mybatis.controller;

import com.herr.springboot.data.mybatis.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/selectById")
    public Object selectById(long id){
        return roleService.selectById(id);
    }

    @GetMapping("/selectById2")
    public Object selectById2(long id){
        return roleService.selectById2(id);
    }

    @GetMapping("/selectById3")
    public Object selectById3(long id){
        return roleService.selectById3(id);
    }

    @GetMapping("/selectRoleById")
    public Object selectRoleById(long id){
        return roleService.selectRoleById(id);
    }

}
