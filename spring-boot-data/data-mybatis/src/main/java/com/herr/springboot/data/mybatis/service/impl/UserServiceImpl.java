package com.herr.springboot.data.mybatis.service.impl;

import com.herr.springboot.data.mybatis.mapper.UserMapper;
import com.herr.springboot.data.mybatis.model.User;
import com.herr.springboot.data.mybatis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User selectById(long userId) {
        User user=userMapper.selectById2(userId);
        return user;
    }

    @Override
    public User findByName(String name) {
        User user=userMapper.findByName2(name);
        return user;
    }

    @Override
    public User findByNameAndPhone(String name, String phone) {
        User user=userMapper.findByNameAndPhone3(name,phone);
        return user;
    }

    @Override
    public int deleteById(long userId) {
        return userMapper.deleteById(userId);
    }

    @Override
    public int insert(User user){
        int flag=userMapper.insert4(user);
        //当useGeneratedKeys，user可查看到主键id
        System.out.println(user);
        if (user.getUserId()==null)
            return flag;
        else
            return user.getUserId().intValue();
    }

    @Override
    public int insertSelective(User user) {
        return userMapper.insertSelective(user);
    }

    @Override
    public int updateById(User user) {
        return userMapper.updateById(user);
    }

    @Override
    public int updateByIdSelective(User user) {
        return userMapper.updateByIdSelective(user);
    }

    @Override
    public List<User> selectUsers() {
        return userMapper.selectUsers();
    }

}
