package com.herr.springboot.data.jpamulti.dao1;

import com.herr.springboot.data.jpamulti.entity.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
    List<Person> findByAddress(String address);

    Person findByNameAndAddress(String name,String address);

    Page<Person> findByName(String name, Pageable pageable);

    @Query("select p from person p where p.name= :name and p.address= :address")
    Person withNameAndAddressQuery(@Param("name")String name, @Param("address")String address);


}