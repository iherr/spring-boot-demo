package com.herr.springboot.data.jpamulti.controller;

import com.herr.springboot.data.jpamulti.dao1.PersonRepository;
import com.herr.springboot.data.jpamulti.dao2.PersonRepository2;
import com.herr.springboot.data.jpamulti.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {
    @Autowired
    PersonRepository personRepository;

    @Autowired
    PersonRepository2 personRepository2;

    @RequestMapping("/findById")
    public Person findById(String id){
        Person person = personRepository.findById(Long.parseLong(id)).get();
        return person;
    }

    @RequestMapping("/findById2")
    public Person findById2(String id){
        Person person = personRepository2.findById(Long.parseLong(id)).get();
        return person;
    }

    //@RequestMapping("/findAll")
    //public List<Person> findAll(){
    //
    //    List<Person> pagePeople = personRepository.findAll(new Sort(Sort.Direction.DESC,"age"));
    //
    //    return pagePeople;
    //
    //}

    //@RequestMapping("/findByName")
    //public Page<Person> findByName(){
    //
    //    Page<Person> pagePeople = personRepository.findByName("herr", new PageRequest(0,10));
    //    return pagePeople;
    //
    //}

    @RequestMapping("/findByAddress")
    public List<Person> findByAddress(String address){
        List<Person> pagePeople = personRepository.findByAddress(address);
        return pagePeople;
    }

    //@RequestMapping("/findByNameAndAddress")
    //public Person findByNameAndAddress(String name,String address){
    //
    //    Person pagePeople = personRepository.withNameAndAddressNamedQuery(name,address);
    //
    //    return pagePeople;
    //
    //}

}