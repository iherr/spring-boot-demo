package com.herr.springboot.data.jpamulti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataJpaMultiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataJpaMultiApplication.class, args);
    }
}
