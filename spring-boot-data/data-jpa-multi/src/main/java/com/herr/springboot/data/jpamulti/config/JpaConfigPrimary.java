package com.herr.springboot.data.jpamulti.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;


@Configuration
@EnableJpaRepositories(basePackages = "com.herr.springboot.data.jpamulti.dao1",entityManagerFactoryRef = "primaryLocalContainerEntityManagerFactoryBean",transactionManagerRef = "primaryPlatformTransactionManager")
public class JpaConfigPrimary {
    @Autowired
    @Qualifier("dsPrimary")
    DataSource dsPrimary;

    @Autowired
    JpaProperties jpaProperties;

    @Bean
    @Primary
    LocalContainerEntityManagerFactoryBean primaryLocalContainerEntityManagerFactoryBean(EntityManagerFactoryBuilder builder) {
        return builder.dataSource(dsPrimary)
                .properties(jpaProperties.getProperties())
                .persistenceUnit("pu1")
                .packages("com.herr.springboot.data.jpamulti.entity")
                .build();
    }

    @Bean
    PlatformTransactionManager primaryPlatformTransactionManager(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(primaryLocalContainerEntityManagerFactoryBean(builder).getObject());
    }
}
