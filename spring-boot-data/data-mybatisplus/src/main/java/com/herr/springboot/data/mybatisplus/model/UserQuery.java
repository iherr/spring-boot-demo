package com.herr.springboot.data.mybatisplus.model;

import javax.validation.constraints.NotNull;

/**
 * 用户查询入参
 *
 * @author herr05
 */

public class UserQuery {
    @NotNull(message = "usercode不能为空")
    private String usercode;

    @NotNull(message = "pwd不能为空")
    private String pwd;

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
