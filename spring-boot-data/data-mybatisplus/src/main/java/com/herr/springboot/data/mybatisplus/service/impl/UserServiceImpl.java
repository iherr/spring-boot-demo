package com.herr.springboot.data.mybatisplus.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.herr.springboot.data.mybatisplus.mapper.UserMapper;
import com.herr.springboot.data.mybatisplus.model.PageQuery;
import com.herr.springboot.data.mybatisplus.model.User;
import com.herr.springboot.data.mybatisplus.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户表 服务实现类
 *
 * @author herr05
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public IPage<User> listPage(PageQuery pageVo, String userName) {
        Page<User> page=new Page<>(pageVo.getPageid(),pageVo.getPagesize());
        page.setOptimizeCountSql(false);
        page.setSearchCount(false);
        return page.setRecords(userMapper.listPage(page,userName).getRecords());
    }
}
