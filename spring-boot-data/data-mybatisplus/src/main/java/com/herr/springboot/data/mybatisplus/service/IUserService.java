package com.herr.springboot.data.mybatisplus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.herr.springboot.data.mybatisplus.model.PageQuery;
import com.herr.springboot.data.mybatisplus.model.User;

/**
 * 用户表 服务类
 *
 * @author herr05
 */
public interface IUserService extends IService<User> {
    IPage<User> listPage(PageQuery pageVo, String userName);
}
