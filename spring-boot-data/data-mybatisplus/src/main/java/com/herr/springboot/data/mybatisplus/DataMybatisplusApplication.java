package com.herr.springboot.data.mybatisplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */
@SpringBootApplication
public class DataMybatisplusApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataMybatisplusApplication.class, args);
    }
}
