package com.herr.springboot.data.cluster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author herr05
 */
@SpringBootApplication
@EnableCaching
public class DataRedisClusterApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataRedisClusterApplication.class,args);
    }
}
