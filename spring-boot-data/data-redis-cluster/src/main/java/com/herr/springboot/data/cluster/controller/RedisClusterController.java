package com.herr.springboot.data.cluster.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */
@RequestMapping("/cluster")
@RestController
public class RedisClusterController {

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/get")
    public String get(){
        String val= (String) redisTemplate.opsForValue().get("name");
        return val;
    }

}
