package com.herr.springboot.data.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author herr05
 */
@RequestMapping("/base")
@RestController
public class RedisBaseController {

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/delete")
    public Boolean delete(){
        Boolean val= redisTemplate.delete("name");
        return val;
    }

    @RequestMapping("/shuffle")
    public String shuffle(){
        Stream<String> stream1 = Stream.iterate("1", x->x).limit(10);
        Stream<String> stream2 = Stream.iterate("2", x->x).limit(10);
        Stream<String> stream3 = Stream.iterate("3", x->x).limit(10);
        List<String> list = stream1.collect(Collectors.toList());
        list.addAll(stream2.collect(Collectors.toList()));
        list.addAll(stream3.collect(Collectors.toList()));
        Collections.shuffle(list);
        list.forEach(x-> redisTemplate.opsForList().rightPush("hb_1",x));
        return "shuffle";
    }

}
