package com.herr.springboot.data.redis.controller;

import com.herr.springboot.data.redis.entity.User;
import com.herr.springboot.data.redis.service.Cache1Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */

/**
 * 如果是单机应用，缓存可通过java写的ehcache实现
 */
@RestController
@RequestMapping("/cache")
public class RedisCacheController {
    @Autowired
    private Cache1Service cache1Service;

    @RequestMapping("user1")
    public User getUser1(Integer id){
        User user = cache1Service.getUser(id);
        return user;
    }

    @RequestMapping("user2")
    @Cacheable(cacheNames = "user2")
    public User getUser2(){
        User user = new User("herr", 18);
        return user;
    }

    @RequestMapping("setUser")
    public User setUser(){
        User user = cache1Service.setUser(new User(1,"herr", 19));
        return user;
    }

    @RequestMapping("delUser")
    public String delUser(){
        cache1Service.delUser(1);
        return "ok";
    }
}


