package com.herr.springboot.data.redis.service;

import com.herr.springboot.data.redis.entity.User;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author herr05
 */

@Service
//cacheNames可修改成redis-cache2，启用RedisCacheConfig中的配置2
@CacheConfig(cacheNames = "redis-cache1", cacheManager = "redisCacheManager")
public class Cache1Service {

    /**
     * 先去redis查询，如没有则执行逻辑，返回数据时放redis缓存
     */
    @Cacheable(cacheNames = "redis-cache1", key = "#id", sync = true)
    public User getUser(Integer id) {
        System.out.println("查询user");
        return new User("herr", 18);
    }

    /**
     * 缓存结果
     */
    @CachePut(key ="#user.getId()")
    public User setUser(User user){
        System.out.println("存入user");
        return user;
    }

    @CacheEvict(key = "#id")
    public void delUser(Integer id) {
        System.out.println("删除user");
    }

}
