package com.herr.springboot.data.redis.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpSession;

/**
 * 添加依赖spring-session-data-redis
 * 用于session的redis共享
 * @author herr05
 */
@RestController
@RequestMapping("/session")
public class RedisSessionController {
    @RequestMapping("/set")
    public String set(HttpSession session) {
        session.setAttribute("name", "herr");
        return "sucesss";
    }

    @RequestMapping("/get")
    public String get(HttpSession session) {
        return ((String) session.getAttribute("name")) ;
    }

}
