package com.herr.springboot.data.redis;

/**
 * @author herr05
 */
public class RandomValueUtil {
    public static String random(){
        int max=1000,min=1;
        int ran = (int) (Math.random()*(max-min)+min);
        return String.valueOf(ran);
    }
}
