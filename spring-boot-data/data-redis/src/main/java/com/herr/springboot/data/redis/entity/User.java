package com.herr.springboot.data.redis.entity;

import java.io.Serializable;

/**
 * @author herr05
 */
public class User implements Serializable {
    private static final long serialVersionUID = 6249829059822088501L;
    private int id;
    private String name;
    private int age;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // redis序列化，使用的是无参构造器并set方法进行赋值，需要补全。不然报错
    // SerializationException: Could not read JSON: Cannot construct instance of `User` (no Creators, like default construct, exist)
    public User() {
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public User(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
