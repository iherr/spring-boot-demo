package com.herr.springboot.data.redis.controller;

import com.herr.springboot.data.redis.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */
@RequestMapping("/string")
@RestController
public class RedisStringController {
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/set")
    public String set(){
        redisTemplate.opsForValue().set("name","herr");
        return "ok";
    }
    @RequestMapping("/get")
    public String get(){
        String name =(String) redisTemplate.opsForValue().get("name");
        return name;
    }

    @RequestMapping("/setuser")
    public String setuser(){
        User user = new User("herr", 18);
        redisTemplate.opsForValue().set("user",user);
        return "ok";
    }
    @RequestMapping("/getuser")
    public String getuser(){
        User name =(User) redisTemplate.opsForValue().get("user");
        return name.toString();
    }

}
