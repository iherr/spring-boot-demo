package com.herr.springboot.data.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * @author herr05
 */
@RequestMapping("/set")
@RestController
public class RedisSetController {
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/sadd")
    public Long sadd(){
        Long count = redisTemplate.opsForSet().add("myset","t1","t2","t3");
        return count;
    }


    @RequestMapping("/smembers")
    public Set<String> smembers(){
        Set<String> set = (Set<String>)redisTemplate.opsForSet().members("myset");
        return set;
    }

}
