package com.herr.springboot.data.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */
@RequestMapping("/hash")
@RestController
public class RedisHashController {
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/hset")
    public String hset(){
        redisTemplate.opsForHash().put("myhash","name","herr");
        return "OK";
    }

    @RequestMapping("/hget")
    public String hget(){
        String str =(String) redisTemplate.opsForHash().get("myhash", "name");
        return str;
    }
}
