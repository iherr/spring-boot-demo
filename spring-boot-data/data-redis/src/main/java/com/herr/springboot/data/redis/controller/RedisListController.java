package com.herr.springboot.data.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author herr05
 */

@RequestMapping("/list")
@RestController
public class RedisListController {
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/lpush")
    public String lpush(){
        int max=100,min=1;
        int ran = (int) (Math.random()*(max-min)+min);
        String test = String.valueOf(ran);
        Long count= redisTemplate.opsForList().leftPush("bucket",test);
        return String.valueOf(count);
    }

    @RequestMapping("/lpop")
    public String lpop(){
        String val= (String) redisTemplate.opsForList().leftPop("bucket");
        return val;
    }

    @RequestMapping("/lrange")
    public List<String> lrange(){
        List<String> val= (List<String>) redisTemplate.opsForList().range("bucket",0,-1);
        return val;
    }

}
