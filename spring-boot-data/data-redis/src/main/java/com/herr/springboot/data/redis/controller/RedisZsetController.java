package com.herr.springboot.data.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * @author herr05
 */
@RequestMapping("/zset")
@RestController
public class RedisZsetController {
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/zadd")
    public Boolean zadd(){
        Boolean b = redisTemplate.opsForZSet().add("myzset","z1",1);
        return b;
    }

    @RequestMapping("/zrange")
    public Set<String> zrange(){
        Set<String> zset = (Set<String>)redisTemplate.opsForZSet().range("myzset",0,-1);
        return zset;
    }
}
