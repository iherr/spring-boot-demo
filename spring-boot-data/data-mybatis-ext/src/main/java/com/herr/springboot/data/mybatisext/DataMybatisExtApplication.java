package com.herr.springboot.data.mybatisext;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataMybatisExtApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataMybatisExtApplication.class, args);
    }
}
