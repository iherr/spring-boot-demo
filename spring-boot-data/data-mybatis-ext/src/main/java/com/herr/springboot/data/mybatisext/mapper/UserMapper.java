package com.herr.springboot.data.mybatisext.mapper;

import com.herr.springboot.data.mybatisext.model.User;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

@Mapper
// 该注解用于规避IDEA mapper注入时warn显示could not autowire，无实际作用
@Component(value = "userMapper")
//可使用CacheNamespace来实现二级缓存
//@CacheNamespace
public interface UserMapper extends BaseMapper<User> {

    User selectById(Integer userId);

    User selectById2(Integer userId);

    int insert(User user);

    List<User> selectUsers();

}