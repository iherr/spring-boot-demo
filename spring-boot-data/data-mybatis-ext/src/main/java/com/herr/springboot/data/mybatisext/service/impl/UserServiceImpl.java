package com.herr.springboot.data.mybatisext.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.herr.springboot.data.mybatisext.mapper.UserMapper;
import com.herr.springboot.data.mybatisext.model.User;
import com.herr.springboot.data.mybatisext.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User selectByPrimaryKey(int primarykey) {
        User user = userMapper.selectByPrimaryKey(primarykey);
        return user;
    }

    @Override
    public User selectById(int primarykey) {
        User user = userMapper.selectById(primarykey);
        return user;
    }

    /*
     * 用到分页插件pagehelper，只需要在service层传入参数，然后将参数传递给一个插件的一个静态方法即可；
     * pageNum 开始页数
     * pageSize 每页显示的数据条数
     * */
    @Override
    public PageInfo<User> findAllUser(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<User> user = userMapper.selectUsers();
        PageInfo result = new PageInfo(user);
        return result;
    }

}
