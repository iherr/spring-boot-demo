package com.herr.springboot.data.mybatisext.service;

import com.github.pagehelper.PageInfo;
import com.herr.springboot.data.mybatisext.model.User;

public interface UserService {

    User selectByPrimaryKey(int primarykey);

    User selectById(int primarykey);

    PageInfo<User> findAllUser(int pageNum, int pageSize);

}
