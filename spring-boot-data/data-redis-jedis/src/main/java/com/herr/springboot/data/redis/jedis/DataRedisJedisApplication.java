package com.herr.springboot.data.redis.jedis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */
@SpringBootApplication
public class DataRedisJedisApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataRedisJedisApplication.class,args);
    }
}
