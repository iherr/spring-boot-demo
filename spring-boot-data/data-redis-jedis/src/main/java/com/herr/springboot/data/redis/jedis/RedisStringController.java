package com.herr.springboot.data.redis.jedis;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;
import java.util.UUID;

/**
 * @author herr05
 */
@RequestMapping("/string")
@RestController
public class RedisStringController {
    RedisClient client = new RedisClient(0);
    RedisClient client2 = new RedisClient(1);
    RedisClient client3 = new RedisClient(2);
    RedisClient client4 = new RedisClient(3);
    RedisClient client5 = new RedisClient(4);
    RedisClient client6 = new RedisClient(5);
    RedisClient client7 = new RedisClient(6);
    RedisClient client8 = new RedisClient(7);
    RedisClient client9 = new RedisClient(8);
    RedisClient client10 = new RedisClient(9);

    @RequestMapping("/set")
    public String set(){
        client.set("name","herr");
        client2.set("name","herr2");
        client3.set("name","herr3");
        client4.set("name","herr4");
        client5.set("name","herr5");
        client6.set("name","herr6");
        client7.set("name","herr7");
        client8.set("name","herr8");
        client9.set("name","herr9");
        client10.set("name","herr10");
        return "ok";
    }
    @RequestMapping("/get")
    public String get(){
        String uuid = UUID.randomUUID().toString();
        Random r = new Random();
        int index=r.nextInt(10);
        System.out.println(uuid+":"+index);
        //RedisClientForSim client1 = new RedisClientForSim();
        RedisClient client2 = new RedisClient(index);
        String name =client2.get("name",uuid);
        //System.out.println(uuid+":"+name);
        return name;
    }

}
