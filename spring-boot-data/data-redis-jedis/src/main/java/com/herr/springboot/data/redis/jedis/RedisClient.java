package com.herr.springboot.data.redis.jedis;

import redis.clients.jedis.Jedis;


public class RedisClient {

	
	private int index = 0;
	private RedisClient() {
	}

	public RedisClient(int db_index) {
		index = db_index;
	}

    public Boolean set(String key,String value){
    	Jedis jedis = null;
    	Boolean flag = false;
    	try{
    		jedis = RedisPool.getJedis(index);
    		flag =jedis.set(key,value).equals("OK")?true:false;
    	}finally{
    		if(null!=jedis){
    			jedis.close();
    		}
    	}
    	return flag;
    }

	public String get(String key,String uuid){
		Jedis jedis = null;
		String flag = null;
		try{
			jedis = RedisPool.getJedis(index,uuid);
			flag =jedis.get(key);
		}finally{
			if(null!=jedis){
				jedis.close();
			}
		}
		return flag;

	}

}
