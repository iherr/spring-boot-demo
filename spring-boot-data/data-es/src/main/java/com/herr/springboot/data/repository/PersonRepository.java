package com.herr.springboot.data.repository;


import com.herr.springboot.data.model.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * 用户持久层
 */
public interface PersonRepository extends ElasticsearchRepository<User, Long> {

    /**
     * 根据年龄区间查询
     *
     * @param min 最小值
     * @param max 最大值
     * @return 满足条件的用户列表
     */
    List<User> findByAgeBetween(Integer min, Integer max);
}
