package com.herr.springboot.data.jpa.controller;

import com.herr.springboot.data.jpa.dao.PersonRepository;
import com.herr.springboot.data.jpa.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {
    @Autowired
    PersonRepository personRepository;

    @RequestMapping("/findById")
    public Person auto(String id){

        Person pagePeople = personRepository.findById(Long.parseLong(id)).get();

        return pagePeople;

    }

    //@RequestMapping("/findAll")
    //public List<Person> findAll(){
    //
    //    List<Person> pagePeople = personRepository.findAll(new Sort(Sort.Direction.DESC,"age"));
    //
    //    return pagePeople;
    //
    //}

    //@RequestMapping("/findByName")
    //public Page<Person> findByName(){
    //
    //    Page<Person> pagePeople = personRepository.findByName("herr", new PageRequest(0,10));
    //    return pagePeople;
    //
    //}

    @RequestMapping("/findByAddress")
    public List<Person> findByAddress(String address){

        List<Person> pagePeople = personRepository.findByAddress(address);

        return pagePeople;

    }

    @RequestMapping("/findByNameAndAddress")
    public Person findByNameAndAddress(String name,String address){

        Person pagePeople = personRepository.withNameAndAddressNamedQuery(name,address);

        return pagePeople;

    }


    @RequestMapping("/findId")
    public Person findId(Long id){

        Person pagePeople = personRepository.withIdQuery(id);

        return pagePeople;

    }

}