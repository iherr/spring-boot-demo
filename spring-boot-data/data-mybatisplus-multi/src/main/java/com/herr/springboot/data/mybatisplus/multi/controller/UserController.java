package com.herr.springboot.data.mybatisplus.multi.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.herr.springboot.data.mybatisplus.multi.model.PageQuery;
import com.herr.springboot.data.mybatisplus.multi.model.User;
import com.herr.springboot.data.mybatisplus.multi.model.UserQuery;
import com.herr.springboot.data.mybatisplus.multi.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户表 前端控制器
 *
 * @author herr05
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {

    private final static Logger log = LoggerFactory.getLogger(UserController.class.getName());

    @Autowired
    private IUserService userService;

    /**
     * 添加用户
     * @param userQuery
     * @return
     */
    @PutMapping("")
    public String addUser(@Validated UserQuery userQuery){
        User user=new User();
        user.setUserName(userQuery.getUsercode());
        userService.save(user);
        log.info("插入后的主键id："+ user.getUserId().toString());
        return "success";
    }

    /**
     *
     * @param pageVo 分页
     * @return
     */
    @GetMapping("/list")
    public List<User> listPage(PageQuery pageVo){
        IPage<User> pages = userService.listPage(pageVo,"herr");
        List<User> userList=pages.getRecords();
        return userList;
    }


    /**
     * 查询某用户信息
     * @param userId
     * @return
     */
    @GetMapping("/{userId}")
    public User getUser(@PathVariable("userId") Integer userId){
        User user =userService.getOne(Wrappers.<User>lambdaQuery()
                .eq(User::getUserId,userId));
        return user;
    }

}
