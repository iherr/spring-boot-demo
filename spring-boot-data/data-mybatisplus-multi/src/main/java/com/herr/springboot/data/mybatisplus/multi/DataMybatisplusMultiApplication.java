package com.herr.springboot.data.mybatisplus.multi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */
@SpringBootApplication
public class DataMybatisplusMultiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataMybatisplusMultiApplication.class, args);
    }
}
