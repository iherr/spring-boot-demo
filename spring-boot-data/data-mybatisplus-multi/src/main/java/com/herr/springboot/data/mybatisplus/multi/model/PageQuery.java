package com.herr.springboot.data.mybatisplus.multi.model;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 分页入参
 * @author herr05
 */


public class PageQuery {
    @Min(value=1, message = "pageid只能大于等于1")
    @NotNull(message = "pageid不能为空")
    private Integer pageid;

    @Range(min=1,max=100, message = "pagesize只能为1-100之间")
    @NotNull(message = "pagesize不能为空")
    private Integer pagesize;

    public Integer getPageid() {
        return pageid;
    }

    public void setPageid(Integer pageid) {
        this.pageid = pageid;
    }

    public Integer getPagesize() {
        return pagesize;
    }

    public void setPagesize(Integer pagesize) {
        this.pagesize = pagesize;
    }
}
