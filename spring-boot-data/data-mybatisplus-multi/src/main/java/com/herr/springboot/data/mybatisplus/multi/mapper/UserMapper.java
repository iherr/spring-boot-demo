package com.herr.springboot.data.mybatisplus.multi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.herr.springboot.data.mybatisplus.multi.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 用户表 Mapper接口
 *
 * @author herr05
 */
@Mapper
@Component(value = "userMapper")
public interface UserMapper extends BaseMapper<User> {

    IPage<User> listPage(Page<User> page, @Param("userName") String userName);

}
