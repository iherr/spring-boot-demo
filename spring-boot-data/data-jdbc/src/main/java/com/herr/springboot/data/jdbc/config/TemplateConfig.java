package com.herr.springboot.data.jdbc.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * 默认的jdbcTemplate仅在单数据源（无自定义Bean）的情况下才会自动配置
 * 可以注释掉@Configuration，走默认配置方式
 * @author herr05
 */
//@Configuration
public class TemplateConfig {

    @Bean("dsOne")
    @ConfigurationProperties(prefix = "spring.ds.one")
    public DataSource dsOne() {
        return new HikariDataSource();
    }

    @Bean("jdbcTemplateOne")
    public JdbcTemplate jdbcTemplateOne(@Qualifier("dsOne") DataSource dsOne) {
        return new JdbcTemplate(dsOne);
    }

    @Bean("dsSecond")
    @ConfigurationProperties(prefix = "spring.ds.second")
    public DataSource dsSecond() {
        return new HikariDataSource();
    }

    //@Bean("jdbcTemplateSecond")
    //public JdbcTemplate jdbcTemplateSecond() {
    //    return new JdbcTemplate(dsSecond());
    //}

    @Bean("jdbcTemplateSecond")
    public JdbcTemplate jdbcTemplateSecond(@Qualifier("dsSecond") DataSource dsSecond) {
        return new JdbcTemplate(dsSecond);
    }

}
