package com.herr.springboot.data.jdbc.controller;

import com.herr.springboot.data.jdbc.entity.User;
import com.herr.springboot.data.jdbc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    @Qualifier("userServiceImpl2")
    private UserService userSerivce;


    @RequestMapping(value = "/create")
    public String create(){
        userSerivce.create("herr", 20);
        return "success";
    }

    @RequestMapping(value = "/all")
    public List<User> all(){
        return userSerivce.getAllUsers();
    }
}
