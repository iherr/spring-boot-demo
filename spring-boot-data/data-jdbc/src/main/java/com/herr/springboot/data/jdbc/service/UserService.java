package com.herr.springboot.data.jdbc.service;


import com.herr.springboot.data.jdbc.entity.User;

import java.util.List;

public interface UserService {

    /**
     * 新增一个用户
     * @param name
     * @param age
     */
    void create(String name, Integer age);
    /**
     * 根据name删除一个用户高
     * @param name
     */
    void deleteByName(String name);
    /**
     * 获取用户总量
     */
    Integer getAllUsersCount();
    /**
     * 删除所有用户
     */
    void deleteAllUsers();

    List<User> getAllUsers();

    List<User> getAllUsers2();
}
