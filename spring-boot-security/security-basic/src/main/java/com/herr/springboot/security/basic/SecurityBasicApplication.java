package com.herr.springboot.security.basic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */
@SpringBootApplication
public class SecurityBasicApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecurityBasicApplication.class,args);
    }
}
