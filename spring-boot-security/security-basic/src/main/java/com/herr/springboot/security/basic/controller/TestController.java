package com.herr.springboot.security.basic.controller;

import com.herr.springboot.security.basic.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */

@RestController
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping("/test")
    public String test() {
        return testService.test();
    }

    //@PreAuthorize("hasRole('admin')")
    @GetMapping("/admin/test")
    public String admintest() {
        return testService.admintest();
    }

    //@PreAuthorize("hasRole('user')")
    @GetMapping("/user/test")
    public String usertest() {
        return testService.usertest();
    }

}
