package com.herr.springboot.security.basic.service;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

/**
 * @author herr05
 */
@Service
public class TestService {

    public String test() {
        return "test";
    }

    public String admintest() {
        return "admin test";
    }

    public String usertest() {
        return "user test";
    }
}
