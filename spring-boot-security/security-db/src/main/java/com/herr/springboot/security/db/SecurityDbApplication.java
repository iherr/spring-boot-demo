package com.herr.springboot.security.db;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */
@SpringBootApplication
public class SecurityDbApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecurityDbApplication.class,args);
    }
}
