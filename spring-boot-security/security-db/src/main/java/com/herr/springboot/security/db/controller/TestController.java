package com.herr.springboot.security.db.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */

@RestController
public class TestController {

    @GetMapping("/admin/test")
    public String admintest() {
        return "admin test";
    }

    @GetMapping("/user/test")
    public String usertest() {
        return "user test";
    }

}
