package com.herr.springboot.security.db.mapper;

import com.herr.springboot.security.db.model.Role;
import com.herr.springboot.security.db.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface UserMapper {
    User loadUserByUsername(String username);

    List<Role> getUserRolesById(Integer id);
}
