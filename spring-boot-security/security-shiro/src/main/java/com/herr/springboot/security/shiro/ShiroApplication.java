package com.herr.springboot.security.shiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShiroApplication {

    /**
     * http://127.0.0.1:8080/doLogin?username=admin&password=123
     */
    public static void main(String[] args) {
        SpringApplication.run(ShiroApplication.class, args);
    }

}
