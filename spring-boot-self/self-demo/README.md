### Springboot-base说明：

1、ORM
  * 集成MyBatis Plus，拓展MyBatis特性，自动实现单类的CURD和page分页。
  * 通用数据库字段create_time,update_time,del_flag等自动填充处理。

2、日志
  * 使用log4j2异步
  * 完整记录request和response参数，自动添加唯一请求ID方便日志链路查看。
  * 使用p6spy打印可执行的sql语句日志供审计

3、文档
  * 集成Swagger UI，进行公共鉴权header参数设置。

4、连接池
  * 使用hikari连接池，以及参数调优。

5、易用性：
  * 使用mybatis-plus-generator和velocity，根据数据库表字段，自动生成controller/service/mapper/entity等文件。
  * 简单查询语句使用条件构造器。
  * 实体的链式Builder和链式转换。
  * 完善的参数校验和异常处理机制。

6、单元测试：
  * JUnit5

7、库表结构
  * 参考springboot-base.sql

### 项目运行

1. Mysql 5.7，将根目录的xxx.sql数据导入初始化
2. application-dev.yml文件，更改spring.datasource
3. run
4. 接口文档和模拟：http://localhost:8080/swagger-ui.html

### 自动生成controller/service/mapper/entity等文件步骤

1. com.herr.springboot.self.xxx.framework.generator.SuperGenerator类为文件自动生成配置，需要配置数据库连接、生成文件路径配置、模板文件配置等才可使用。
2. 创建“t_”为前缀的表结构，如是其他前缀或无前缀可到SuperGenerator类更改。
3. 生成的entity中，会自动继承com.herr.springboot.self.xxx.entity.BaseModel类，其中createTime、updateTime和delFlag为表公共字段。如需要变更同样需要配置。
4. 在test.java.com.herr.springboot.self.xxx.generate.MysqlGeneratorTest测试类修改对应的表名，直接运行generator方法即可。
5. 模板文件位置/resources/templates/。
5. 默认id为自增，支持自定义ID生成（如雪花算法+UUID）模式。
