package com.herr.springboot.self.xxx.framework;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.herr.springboot.self.xxx.service.IUserService;
import com.herr.springboot.self.xxx.common.exception.BizException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Junit测试父类
 * 提供测试数据的事务和自动回滚，通用方法封装
 * @author herr05
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@Transactional
@Rollback(value=true) // 控制测试数据是否自动回滚
public class BaseControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private IUserService userService;

    // 类的mockMvc和header鉴权信息获取初始化
    private static int initFlag=0;

    protected static MockMvc mockMvc;

    protected static HttpHeaders httpHeaders=new HttpHeaders();

    protected static int successCode=1;

    @BeforeEach
    public void before() throws Exception {
        if (initFlag == 0 ) {
            mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

            MultiValueMap<String, String> params =new LinkedMultiValueMap<String, String>();
            params.add("usercode","herr05");
            params.add("pwd","111111");
            JSONObject jsonObject=getResult(get("/user/login",params));
            httpHeaders.set("token", jsonObject.getString("data"));
            initFlag = 1;
        }
    }

    /**
     * 获取MockMvcRequestBodyBuilders
     *
     * @param method
     * @param url
     * @param object
     * @param params
     * @return
     */
    private MockHttpServletRequestBuilder getMockMvcRequestBodyBuilder(HttpMethod method, String url, Object object, MultiValueMap<String, String> params){
        MockHttpServletRequestBuilder requestBuilder;
        switch (method) {
            case GET:
                requestBuilder = MockMvcRequestBuilders.get(url);
                if (Objects.nonNull(params)) {
                    requestBuilder.params(params);
                }
                break;
            case POST:
                requestBuilder = MockMvcRequestBuilders.post(url).contentType(MediaType.APPLICATION_JSON);
                if (Objects.nonNull(object)) {
                    requestBuilder.content(JSON.toJSONString(object));
                }
                break;
            case PUT:
                requestBuilder = MockMvcRequestBuilders.put(url).contentType(MediaType.APPLICATION_JSON);
                if (Objects.nonNull(object)) {
                    requestBuilder.content(JSON.toJSONString(object));
                }
                break;
            case PATCH:
                requestBuilder = MockMvcRequestBuilders.patch(url).contentType(MediaType.APPLICATION_JSON);
                if (Objects.nonNull(object)) {
                    requestBuilder.content(JSON.toJSONString(object));
                }
                break;
            case DELETE:
                requestBuilder = MockMvcRequestBuilders.delete(url);
                if (Objects.nonNull(params)) {
                    requestBuilder.params(params);
                }
                break;
            default:
                throw new BizException("Error: This request method is not supported.");
        }
        requestBuilder.headers(httpHeaders);
        return requestBuilder;
    }

    /**
     * 获取MockMvcRequestBodyBuilders
     *
     * @param url
     * @param params
     * @return
     */
    public MockHttpServletRequestBuilder get(String url, MultiValueMap<String, String> params) {
        return getMockMvcRequestBodyBuilder(HttpMethod.GET, url, null, params);
    }

    /**
     * 获取MockMvcRequestBodyBuilders
     *
     * @param url
     * @return
     */
    public MockHttpServletRequestBuilder get(String url) {
        return getMockMvcRequestBodyBuilder(HttpMethod.GET, url,null, null);
    }

    /**
     * 获取MockMvcRequestBodyBuilders
     *
     * @param url
     * @param object
     * @return
     */
    public MockHttpServletRequestBuilder post(String url, Object object) {
        return getMockMvcRequestBodyBuilder(HttpMethod.POST, url, object, null);
    }

    /**
     * 获取MockMvcRequestBodyBuilders
     *
     * @param url
     * @return
     */
    public MockHttpServletRequestBuilder post(String url) {
        return getMockMvcRequestBodyBuilder(HttpMethod.POST, url, null, null);
    }

    /**
     * 获取MockMvcRequestBodyBuilders
     *
     * @param url
     * @param params
     * @return
     */
    public MockHttpServletRequestBuilder delete(String url, MultiValueMap<String, String> params) {
        return getMockMvcRequestBodyBuilder(HttpMethod.DELETE, url,null, params);
    }

    /**
     * 获取MockMvcRequestBodyBuilders
     *
     * @param url
     * @param object
     * @return
     */
    public MockHttpServletRequestBuilder put(String url, Object object) {
        return getMockMvcRequestBodyBuilder(HttpMethod.PUT, url, object, null);
    }

    /**
     * 获取Mock测试请求结果是否为200
     *
     * @param builder
     * @return
     * @throws Exception
     */
    public ResultActions isOk(MockHttpServletRequestBuilder builder) throws Exception {
        return getResultActions(builder).andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * 获取Mock测试请求结果返回的code是否为1
     *
     * @param builder
     * @return
     * @throws Exception
     */
    public void isSuccess(MockHttpServletRequestBuilder builder) throws Exception {
        assertEquals(successCode,getResult(builder).getIntValue("code"));
    }

    /**
     * 获取ResultActions
     *
     * @param builder
     * @return
     * @throws Exception
     */
    private ResultActions getResultActions(MockHttpServletRequestBuilder builder) throws Exception {
        return mockMvc.perform(builder).andDo(MockMvcResultHandlers.print());
    }

    /**
     * 获取MockMvc测试字符串返回
     *
     * @param builder
     * @return JSON对象
     * @throws Exception
     */
    public JSONObject getResult(MockHttpServletRequestBuilder builder) throws Exception {
        MvcResult mvcResult= mockMvc.perform(builder).andReturn();
        JSONObject result = JSON.parseObject(mvcResult.getResponse().getContentAsString());
        return result;
    }

}
