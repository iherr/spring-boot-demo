package com.herr.springboot.self.xxx.controller;

import com.herr.springboot.self.xxx.framework.BaseControllerTest;
import org.junit.jupiter.api.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * @author herr05
 */
class UserControllerTest extends BaseControllerTest {

    @Test
    void addUser() throws Exception {
        MultiValueMap<String, String> params =new LinkedMultiValueMap<String, String>();
        isSuccess(put("/user?usercode=ceshi01&pwd=111111",params));
    }

    @Test
    void getUser() throws Exception {
        isSuccess(get("/user/1",null));
    }

    @Test
    void deleteUser() throws Exception {
        isSuccess(delete("/user/2",null));
    }

    @Test
    void getLoginUser() throws Exception {
        isSuccess(get("/user/getLoginUser",null));
    }

    @Test
    void listPage() throws Exception {
        MultiValueMap<String, String> params =new LinkedMultiValueMap<>();
        params.add("pageid","1");
        params.add("pagesize","10");
        isSuccess(get("/user/listPage",params));
    }

}