package com.herr.springboot.self.xxx.generate;

import com.herr.springboot.self.xxx.framework.generator.MysqlGenerator;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * 根据tableName按模板（resources/template）
 * 自动生成表对应的controller、service、mapper、entity等文件
 *
 * @author herr05
 *
 */
public class MysqlGeneratorTest {

    /**
     * 打包时该测试Skipped，如需生成类，请单独执行。
     */
    @Test
    @Disabled
    public void generator() {
        MysqlGenerator mysqlGenerator = new MysqlGenerator();
        mysqlGenerator.generator("t_user");
    }
}
