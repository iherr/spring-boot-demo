package com.herr.springboot.self.xxx.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.herr.springboot.self.xxx.mapper.UserMapper;
import com.herr.springboot.self.xxx.model.dto.UserDTO;
import com.herr.springboot.self.xxx.model.entity.User;
import com.herr.springboot.self.xxx.model.query.PageQuery;
import com.herr.springboot.self.xxx.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户表 服务实现类
 *
 * @author herr05
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public IPage<UserDTO> listPage(PageQuery pageVo, String deptName) {
        Page<UserDTO> page=new Page<>(pageVo.getPageid(),pageVo.getPagesize());
        page.setOptimizeCountSql(false);
        page.setSearchCount(false);
        return page.setRecords(userMapper.listPage(page,deptName).getRecords());
    }
}
