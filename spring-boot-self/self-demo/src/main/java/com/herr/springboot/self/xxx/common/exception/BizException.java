package com.herr.springboot.self.xxx.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自定义业务异常
 * @author herr05
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BizException extends RuntimeException {

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private Integer code;
	private String message;
	private Object data;

	public BizException(String message) {
		super(message);
		this.code=1000;
		this.message=message;
	}

	public BizException(Integer code){
		super();
		this.code=code;
	}

	public BizException(Integer code, String message) {
		super(message);
		this.code = code;
		this.message = message;
	}

	public BizException(Integer code, String message, Object data) {
		super(message);
		this.code = code;
		this.message = message;
		this.data = data;
	}
}
