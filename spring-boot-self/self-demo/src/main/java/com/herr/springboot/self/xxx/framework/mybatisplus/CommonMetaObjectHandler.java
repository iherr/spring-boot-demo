package com.herr.springboot.self.xxx.framework.mybatisplus;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.herr.springboot.self.xxx.config.MybatisPlusConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;

import java.time.LocalDateTime;

/**
 * 通用填充类 用于mybatis plus通用字段的自动处理
 *
 * @see MybatisPlusConfig
 * @author herr05
 */
@Slf4j
public class CommonMetaObjectHandler implements MetaObjectHandler {

    /**
     * 创建时间
     */
    private final String createTime = "createTime";
    /**
     * 修改时间
     */
    private final String updateTime = "updateTime";
    /**
     * 删除标记
     */
    private final String delFlag = "delFlag";

    @Override
    public void insertFill(MetaObject metaObject) {
//        log.info("start insert fill ....");
        setInsertFieldValByName(createTime, LocalDateTime.now(), metaObject);
        setInsertFieldValByName(updateTime, LocalDateTime.now(), metaObject);
        setInsertFieldValByName(delFlag, 0, metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        setUpdateFieldValByName(updateTime, LocalDateTime.now(), metaObject);
    }

}
