package com.herr.springboot.self.xxx.model.dto;

import lombok.Data;

/**
 * @author herr05
 */
@Data
public class UserDTO {
    /**
     * 表主键（自增）
     */
    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 用户code
     */
    private String usercode;
    /**
     * 部门名称
     */
    private String deptName;
}
