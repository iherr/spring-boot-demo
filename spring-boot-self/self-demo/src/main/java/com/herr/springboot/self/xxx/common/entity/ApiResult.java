package com.herr.springboot.self.xxx.common.entity;

import com.herr.springboot.self.xxx.common.enums.ApiResultEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 *   业务处理返回 实体类
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiResult<T> implements Serializable {
    /**
     * 成功与失败
     * 0：失败
     * 1：成功
     */
    private Integer code = 1;
    /**
     * 错误信息
     */
    private String msg = "success";
    /**
     * 返回数据
     */
    private T data = null;

    public ApiResult(T t) {
        super();
        this.data = t;
    }

    public ApiResult success() {
        return this;
    }

    public ApiResult success(T t) {
        this.data = t;
        return this;
    }

    public ApiResult fail(String msg) {
        this.code = 0;
        this.msg = msg;
        return this;
    }

    public ApiResult fail(String msg,Integer code) {
        this.code = code;
        this.msg = msg;
        return this;
    }

    public ApiResult fail(ApiResultEnum resultEnum) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
        return this;
    }
}
