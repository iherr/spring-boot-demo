package com.herr.springboot.self.xxx.common.enums;

/**
 * 返回参数枚举
 *
 * @author herr05
 */
public enum ApiResultEnum {
    SUCCESS(0, "成功"),
    BIZ_FAIL(1000, "业务异常"),
    AUTH_FAIL(1001, "鉴权失败"),
    TIMED_OUT(1002, "已超时，请重新登录"),
    OPERATION_FAIL(1003, "操作失败"),
    SYSTEM_BUSY(1004, "系统繁忙，请稍候尝试"),
    PARAM_ERR(1005, "参数校验失败"),
    UNKONW_ERR(1006, "未知错误"),
    MYSQL_ERR(2001, "MYSQL服务不可用"),
    MONGO_ERR(2002, "MONGO服务不可用"),
    REDIS_ERR(2003, "REDIS服务不可用"),
    WECHAT_ERR(2004, "微信服务不可用"),
    ACCOUNT_ERR(4001, "帐号密码有误"),
    ACCOUNT_UPWDERR(4002, "密码有误"),
    PERMISSION_DENIED(4003, "权限不足"),
    AUTH_OVERFLOW(4004, "资源分配溢权"),
    AUTH_ASSIGNERR(4005, "资源分配失败"),
    APP_UNABLE_DELETE(4006, "正在使用,无法删除"),
    APP_NAME_NOTEMPTY(4007, "名称不能为空"),
    APP_EMPTY(4008, "资源不存在");

    private Integer code;

    private String msg;

    ApiResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
