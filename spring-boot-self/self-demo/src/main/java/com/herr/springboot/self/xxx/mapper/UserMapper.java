package com.herr.springboot.self.xxx.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.herr.springboot.self.xxx.model.dto.UserDTO;
import com.herr.springboot.self.xxx.model.entity.User;
import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 用户表 Mapper接口
 *
 * @author herr05
 */
@Mapper
@Component(value = "userMapper")
public interface UserMapper extends BaseMapper<User> {

    IPage<UserDTO> listPage(Page<UserDTO> page, @Param("deptName") String deptName);

}
