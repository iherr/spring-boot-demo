package com.herr.springboot.self.xxx.common.entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * 接口响应日志 实体类
 * @author herr05
 */
@Getter
@ToString
@Builder
@EqualsAndHashCode(callSuper = false)
public class ApiResponseLog {
    /**
     * 请求id，传入，和ApirequestLog对应
     */
    private String requestId;
    /**
     * 请求路径
     */
    private String requestUri;
    /**
     * 请求方法
     */
    private String requestMethod;
    /**
     * 日志需要打印的json字符串
     */
    private Object result;
    /**
     * 接口运行时间 单位:ms
     */
    private String runTime;
}
