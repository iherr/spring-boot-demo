package com.herr.springboot.self.xxx.config;

import com.herr.springboot.self.xxx.common.entity.Principal;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Swagger文档配置和初始化
 *
 * @author herr05
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${application.swagger.showui:true}")
    private boolean showui;

    @Bean
    public Docket api() {
        if (showui) {
            return new Docket(DocumentationType.SWAGGER_2)
                .globalOperationParameters(getParameters())
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(false)
                .ignoredParameterTypes(Principal.class);
        }else {
            return new Docket(DocumentationType.SWAGGER_2)
                    .apiInfo(apiInfo())
                    .select()
                    .paths(PathSelectors.none()).build().useDefaultResponseMessages(false);
        }
    }

    /**
     * 获取swagger ApiInfo
     *
     * @return
     */
    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("xxx API文档")
                .termsOfServiceUrl("")
                .version("1.0")
                .contact(new Contact("xxx", "", "xxx@mail.com"))
                .build();
    }

    /**
     * 获取Swagger参数
     *
     * @return
     */
    List<Parameter> getParameters() {
        return new ArrayList<Parameter>(Arrays.asList(
                new ParameterBuilder()
                        .name("token")
                        .description("鉴权信息")
                        .modelRef(new ModelRef("string"))
                        .parameterType("header")
                        .required(true)
                        .defaultValue("eyJhbGciOiJIUzI1NiJ9.eyJBUElfVUlEIjoiMSIsImV4cCI6MjA5NDU0MzI2MywiaWF0IjoxNTc2MTQzMjYzLCJBUElfVVNFUkNPREUiOiJoZXJyMDUiLCJqdGkiOiJ0b2tlbklkIn0.rIMdaruVXvGpW5NhNwCTKgMZ5K_NPx1JDSogxKfFnY4")
                        .build()
        ));
    }
}
