package com.herr.springboot.self.xxx.common.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 参数解析器实体类
 * 用于对请求的header做解析，例如根据token获取用户相关信息，并作为参数带入controller。
 * @author herr05
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Principal {
    private Long uid;
    private String usercode;
}
