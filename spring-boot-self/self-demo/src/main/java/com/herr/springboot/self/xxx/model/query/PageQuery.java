package com.herr.springboot.self.xxx.model.query;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 分页入参
 * @author herr05
 */

@Data
public class PageQuery {
    @Min(value=1, message = "pageid只能大于等于1")
    @NotNull(message = "pageid不能为空")
    @ApiModelProperty(value = "第几页，从1开始",required = true)
    private Integer pageid;

    @Range(min=1,max=100, message = "pagesize只能为1-100之间")
    @NotNull(message = "pagesize不能为空")
    @ApiModelProperty(value = "每页条数，最大支持100条",required = true)
    private Integer pagesize;
}
