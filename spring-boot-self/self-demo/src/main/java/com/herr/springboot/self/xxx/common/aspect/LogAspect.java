package com.herr.springboot.self.xxx.common.aspect;

import com.herr.springboot.self.xxx.common.util.LogUtils;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * controller切点，用于日志处理
 * @author herr05
 */
@Aspect
public class LogAspect {

    @Pointcut("execution(public * com.herr.springboot.self.xxx.controller.*Controller.*(..))")
    @SuppressWarnings("EmptyMethod")
    public void pointCut() {
    }

    @AfterReturning(returning = "ret", pointcut = "pointCut()")
    public void doAfterReturning(Object ret) {
        LogUtils.doAfterReturning(ret);
    }

    //异常的捕获和日志打印，在common.exception.ControllerAdvice中处理
//    @AfterThrowing(throwing="ex",  pointcut = "pointCut()")
//    public void doAfterThrowing(Exception ex) {
//        LogUtils.doAfterException(ex);
//    }

}
