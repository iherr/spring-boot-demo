package com.herr.springboot.self.xxx.model.query;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 用户查询入参
 *
 * @author herr05
 */
@Data
public class UserQuery {
    @NotNull(message = "usercode不能为空")
    @ApiModelProperty(value = "用户名",required = true)
    private String usercode;

    @NotNull(message = "pwd不能为空")
    @ApiModelProperty(value = "密码",required = true)
    private String pwd;
}
