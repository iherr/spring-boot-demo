package com.herr.springboot.self.xxx.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.herr.springboot.self.xxx.common.entity.BaseModel;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


/**
 * 用户表 实体类
 *
 * @author herr05
 */
@TableName("t_user")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class User extends BaseModel {

    private String usercode;
    private String pwd;
    private String username;
    private String deptName;

}
