package com.herr.springboot.self.xxx.common.constant;

/**
 * 接口 常量
 * @author herr05
 */
public interface APIConstant {
    String API_BEGIN_TIME = "API_BEGIN_TIME";
    String API_GUID = "API_GUID";

    String API_UID="API_UID";
    String API_USERCODE="API_USERCODE";
}
