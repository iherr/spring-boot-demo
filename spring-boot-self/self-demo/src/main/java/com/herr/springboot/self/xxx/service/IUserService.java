package com.herr.springboot.self.xxx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.herr.springboot.self.xxx.model.dto.UserDTO;
import com.herr.springboot.self.xxx.model.entity.User;
import com.herr.springboot.self.xxx.model.query.PageQuery;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户表 服务类
 *
 * @author herr05
 */
public interface IUserService extends IService<User> {
    IPage<UserDTO> listPage(PageQuery pageVo, String deptName);
}
