package com.herr.springboot.self.xxx.common.exception;

/**
 * 自定义鉴权失败异常
 * @author herr05
 */
public class UnAuthException extends RuntimeException {
    public UnAuthException(){
        super();
    }
    public UnAuthException(String message){
        super(message);
    }
    public UnAuthException(Throwable throwable){
        super(throwable);
    }
    public UnAuthException(String message,Throwable throwable){
        super(message,throwable);
    }
}
