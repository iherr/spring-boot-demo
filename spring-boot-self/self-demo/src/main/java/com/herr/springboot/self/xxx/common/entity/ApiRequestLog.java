package com.herr.springboot.self.xxx.common.entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Map;

/**
 * 接口请求日志 实体类
 * @author herr05
 */
@Getter
@ToString
@Builder
@EqualsAndHashCode(callSuper = false)
public class ApiRequestLog {
    /**
     * 请求id，生成
     */
    private String requestId;
    /**
     * 请求路径
     */
    private String requestUri;
    /**
     * 请求方法
     */
    private String requestMethod;
    /**
     * 参数，适用于get等请求
     */
    private Map<String, String> requestHeaders;
    /**
     * 参数，适用于get等请求
     */
    private Map<String, String[]> requestParams;
    /**
     * 参数，适用于post、put等请求
     */
    private Object requestBody;
    /**
     * IP地址
     */
    private String requestIp;
}
