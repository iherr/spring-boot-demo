package com.herr.springboot.self.xxx.framework.generator;

import com.baomidou.mybatisplus.generator.AutoGenerator;

/**
 * Mysql代码生成器
 *
 * @author herr05
 */
public class MysqlGenerator extends SuperGenerator {

    /**
     * MySQL generator
     */
    public void generator(String tableName) {

        // 代码生成器
        AutoGenerator mpg = getAutoGenerator(tableName);
        mpg.execute();
        if (tableName == null) {
            System.err.println(" Generator Success !");
        } else {
            System.err.println(" TableName【 " + tableName + " 】" + "Generator Success !");

        }
    }


}
