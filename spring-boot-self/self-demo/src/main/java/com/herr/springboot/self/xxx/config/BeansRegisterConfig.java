package com.herr.springboot.self.xxx.config;

import com.herr.springboot.self.xxx.common.aspect.LogAspect;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * 常用Bean自注册
 * @author herr05
 */
@Configuration
public class BeansRegisterConfig {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder){
        return builder.build();
    }

    @Bean
    public LogAspect logAspect() {
        return new LogAspect();
    }
}
