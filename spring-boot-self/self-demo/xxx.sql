/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50727
Source Host           : localhost:3306
Source Database       : xxx

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2019-12-13 14:05:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `usercode` varchar(20) NOT NULL,
  `pwd` varchar(32) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `dept_name` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `del_flag` tinyint(1) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'test01', '96e79218965eb72c92a549dd5a330112', 'test00', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('2', 'test01', '96e79218965eb72c92a549dd5a330112', 'test01', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('3', 'test02', '96e79218965eb72c92a549dd5a330112', 'test02', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('4', 'test03', '96e79218965eb72c92a549dd5a330112', 'test03', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('5', 'test04', '96e79218965eb72c92a549dd5a330112', 'test04', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('6', 'test05', '96e79218965eb72c92a549dd5a330112', 'test05', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('7', 'test06', '96e79218965eb72c92a549dd5a330112', 'test06', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('8', 'test07', '96e79218965eb72c92a549dd5a330112', 'test07', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('9', 'test08', '96e79218965eb72c92a549dd5a330112', 'test08', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('10', 'test09', '96e79218965eb72c92a549dd5a330112', 'test09', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('11', 'test10', '96e79218965eb72c92a549dd5a330112', 'test10', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('12', 'test11', '96e79218965eb72c92a549dd5a330112', 'test11', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
INSERT INTO `t_user` VALUES ('13', 'test12', '96e79218965eb72c92a549dd5a330112', 'test12', '市场部', '2019-12-12 11:21:59', '2019-12-12 11:21:59', '0');
