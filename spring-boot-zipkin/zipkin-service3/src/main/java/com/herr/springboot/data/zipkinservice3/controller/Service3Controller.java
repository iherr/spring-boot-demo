package com.herr.springboot.data.zipkinservice3.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Service3Controller {
    @GetMapping("/service3")
    public String service3() {
        return "hello herr" ;
    }
}
