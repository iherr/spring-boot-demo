package com.herr.springboot.base.aop.aspect;

import java.lang.annotation.*;

/**
 * @author herr05
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyLog {
    String value();
}
