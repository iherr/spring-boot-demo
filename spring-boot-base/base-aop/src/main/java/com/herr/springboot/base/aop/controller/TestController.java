package com.herr.springboot.base.aop.controller;

import com.herr.springboot.base.aop.aspect.MyLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */
@RestController
public class TestController {

    public static final Logger log = LoggerFactory.getLogger(TestController.class);

    @RequestMapping("/test")
    public String test(){
        log.info("method: test");
        return "test";
    }

    @RequestMapping("/test2")
    @MyLog("annotation name")
    public String test2(){
        log.info("method: test2");
        return "test2";
    }
}
