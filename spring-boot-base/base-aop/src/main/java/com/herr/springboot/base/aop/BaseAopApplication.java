package com.herr.springboot.base.aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */

@SpringBootApplication
public class BaseAopApplication {
    public static void main(String[] args) {
        SpringApplication.run(BaseAopApplication.class,args);
    }
}
