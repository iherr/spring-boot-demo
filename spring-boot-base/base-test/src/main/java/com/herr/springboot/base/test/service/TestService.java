package com.herr.springboot.base.test.service;

import org.springframework.stereotype.Service;

/**
 * @author herr05
 */
@Service
public class TestService {
    public String sayHello(String name) {
        return "hello2 " + name;
    }
}
