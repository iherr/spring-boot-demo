package com.herr.springboot.base.test.controller;

import com.herr.springboot.base.test.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 使用idea，如需要启用热更新
 * 1、需要在setting->build->compiler，勾选build project aotomatically
 * 2、registry，设置compiler-automake-allow-when-app-running勾选
 */
@RestController
public class TestController {
    @Autowired
    private TestService testService;

    @GetMapping("/hello")
    public String hello(String name) {
        return testService.sayHello(name);
    }


}