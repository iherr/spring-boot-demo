package com.herr.springboot.base.hello.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${hello.time}")
    private String time;

    @Value("${hello.name}")
    private String name;

    @RequestMapping("/")
    String index(){
        return "hello "+ name +" , good "+ time;
    }

    @RequestMapping(value = "/log")
    String log(){
        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        logger.error("This is an error message");
        return "herr";
    }

    @RequestMapping(value = "/test")
    String test(){
        Runtime rt=Runtime.getRuntime();
        System.out.println("处理器"+rt.availableProcessors());
        System.out.println("空闲内存"+rt.freeMemory());
        System.out.println("总内存"+rt.totalMemory());
        return "test";
    }

    @RequestMapping(value = "/test1")
    String test1() throws InterruptedException {
        System.out.println("begin :" + System.currentTimeMillis());
        Thread.sleep(5000);
        System.out.println("end   :" + System.currentTimeMillis());
        return "test1";
    }

    @RequestMapping(value = "/test2")
    String test2() throws InterruptedException {
        System.out.println("begin2:" + System.currentTimeMillis());
        Thread.sleep(5000);
        System.out.println("end2  :" + System.currentTimeMillis());
        return "test2";
    }

}
