package com.herr.springboot.base.hello;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * @author herr05
 */
@SpringBootApplication
public class Base2Application {
    public static void main(String[] args) {
        //如需要关闭banner，可进行如下配置
        SpringApplicationBuilder builder = new SpringApplicationBuilder(BaseHelloApplication.class);
        SpringApplication springApplication = builder.build();
        springApplication.setBannerMode(Banner.Mode.OFF);
        builder.run(args);
    }
}
