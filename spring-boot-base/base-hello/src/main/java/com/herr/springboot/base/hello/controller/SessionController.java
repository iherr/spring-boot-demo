package com.herr.springboot.base.hello.controller;

import jakarta.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */
@RestController
@RequestMapping("/session")
public class SessionController {
    @RequestMapping("/set")
    public String set(HttpSession session) {
        session.setAttribute("name", "herr");
        return "sucesss";
    }

    @RequestMapping("/get")
    public String get(HttpSession session) {
        return ((String) session.getAttribute("name"));
    }

}
