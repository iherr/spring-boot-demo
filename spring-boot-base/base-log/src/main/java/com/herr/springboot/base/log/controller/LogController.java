package com.herr.springboot.base.log.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */
@Slf4j
@RestController
public class LogController {
    @RequestMapping("/log")
    public String log(){
        log.info("info");
        log.error("error");
        return "log";
    }
}
