package com.herr.springboot.base.resource.config;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * 自定义资源路径。实现方式有以下几种
 * 1、继承 WebMvcConfigurationSupport，则不会自动配置，原有默认路径映射会失效
 * 2、继承WebMvcConfigurerAdapter仍然可用，会自动配置
 * 3、由于WebMvcConfigurerAdapter过时，可以实现上层接口WebMvcConfigurer，会自动配置
 */
@Configuration
//@EnableWebMvc //完全接管自动配置，则自动配置会失效。
public class ResourceAutoConfig implements WebMvcConfigurer {
//public class ResourceAutoConfig extends WebMvcConfigurerAdapter {

    /** 自动配置的默认映射关系为，以下目录都映射到根路径/**，如文件同名会按顺序访问
     * /META-INF/resources
     * resources
     * static
     * public
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/test/**").addResourceLocations("classpath:/test/");
                //.addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/js/**").addResourceLocations("classpath:/js/");
        //pathPatterns为/**，会覆盖默认资源映射
        //registry.addResourceHandler("/**").addResourceLocations("classpath:/resources/");
    }

    //http://localhost:8080/js/a.js
    //http://localhost:8080/test/c.html
}