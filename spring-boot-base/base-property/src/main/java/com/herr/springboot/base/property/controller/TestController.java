package com.herr.springboot.base.property.controller;

import com.herr.springboot.base.property.YamlPropertySourceFactory;
import com.herr.springboot.base.property.config.DataConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */
@RestController
@RequestMapping("/")
public class TestController {

    //从默认application.yml加载
    @Value("${self.name}")
    private String name;

    //从默认config/application.yml加载
    @Value("${self.addr}")
    private String addr;

    //没有找到使用默认值
    @Value("${self.sex:male}")
    private String sex;

    //从BaseApplication启动代码中设置并加载
    @Value("${application.ip}")
    private String applicationIp;

    //可在idea设置environment variables为application.name=property。如未设置会用默认的base
    @Value("${application.name:base}")
    private String applicationName;

    @Autowired
    private Environment env;

    @RequestMapping("/get")
    public String get(){
        return name;
    }

    @RequestMapping("/get2")
    public String get2(){
        String nameValue = env.getProperty("self.name");
        return nameValue;
    }

    @RequestMapping("/get3")
    public String get3(){
        return sex;
    }

    @RequestMapping("/get4")
    public String get4(){
        return addr;
    }

    @RequestMapping("/getApplicationIp")
    public String getApplicationIp(){
        return applicationIp;
    }

    @RequestMapping("/getApplicationName")
    public String getApplicationName(){
        return applicationName;
    }

}
