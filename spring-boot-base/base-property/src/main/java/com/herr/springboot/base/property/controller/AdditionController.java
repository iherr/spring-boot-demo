package com.herr.springboot.base.property.controller;

import com.herr.springboot.base.property.YamlPropertySourceFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */
@RestController
@RequestMapping("/addtion")
@PropertySource("classpath:init.properties")
@PropertySource(value = {"classpath:init2.yml"},factory = YamlPropertySourceFactory.class)
public class AdditionController {
    //从外部文件init.properties加载
    @Value("${init.name}")
    private String initName;

    //从外部文件init2.age加载
    @Value("${init2.age}")
    private String initAge;

    @RequestMapping("/get4")
    public String get4(){
        return initName;
    }


    @RequestMapping("/getAge")
    public String getAge(){
        return initAge;
    }
}
