package com.herr.springboot.base.property;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @author herr05
 */

@SpringBootApplication
public class BasePropertyApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(BasePropertyApplication.class);
        Map<String,Object> defaultProperties = new HashMap<>();
        defaultProperties.put("application.ip","127.0.0.1");
        application.setDefaultProperties(defaultProperties);
        ConfigurableApplicationContext context =application.run(args);

        System.out.println("-----------");
        System.out.println(context.getEnvironment().getProperty("self.name"));
        System.out.println("-----------");
    }
}
