package com.herr.springboot.base.property.config;

import com.herr.springboot.base.property.entity.MysqlDs;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author herr05
 */
@Component
@ConfigurationProperties(prefix = "ds")
public class DataConfig {
    private String name;
    private String pwd;
    private List<String> hosts;
    private List<MysqlDs> list;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public List<String> getHosts() {
        return hosts;
    }

    public void setHosts(List<String> hosts) {
        this.hosts = hosts;
    }

    public List<MysqlDs> getList() {
        return list;
    }

    public void setList(List<MysqlDs> list) {
        this.list = list;
    }
}
