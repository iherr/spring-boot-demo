package com.herr.springboot.base.property.entity;

/**
 * @author herr05
 */
public class MysqlDs {
    private Integer port;
    private String host;

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
