package com.herr.springboot.base.property.controller;

import com.herr.springboot.base.property.config.DataConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author herr05
 */
@RestController
@RequestMapping("/data")
public class DataController {

    @Autowired
    private DataConfig dataConfig;

    @RequestMapping("/get")
    public DataConfig get(){
        return dataConfig;
    }

}
