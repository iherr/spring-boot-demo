package com.herr.springboot.base.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author herr05
 */
@SpringBootApplication
public class BaseIntegrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaseIntegrationApplication.class, args);
    }
}
