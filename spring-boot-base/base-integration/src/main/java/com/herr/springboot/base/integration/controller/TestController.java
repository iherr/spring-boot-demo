package com.herr.springboot.base.integration.controller;

import com.herr.springboot.base.integration.entity.User;
import com.herr.springboot.base.integration.extend.BizException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author herr05
 */
@RestController
public class TestController {
    public static final Logger log= LoggerFactory.getLogger(TestController.class);

    @RequestMapping("/test")
    public String index(){
        log.info("com.herr.springboot.base.swagger.controller test");
        return "test";
    }

    @RequestMapping("/getexception")
    public String getexception(){
        log.info("com.herr.springboot.base.swagger.controller getexception");
        throw new BizException("biz exception");
    }

    @RequestMapping("/alluser")
    public List<User> alluser() {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setId(i);
            user.setAddr("bj" + i);
            user.setName("herr" + i);
            users.add(user);

        }
        return users;
    }

    /**
     * 如未设置DateConverter会报错
     * Failed to convert value of type 'java.lang.String' to required type 'java.util.Date';
     * nested exception is org.springframework.core.convert.ConversionFailedException:
     * Failed to convert from type [java.lang.String] to type [java.util.Date] for value '2017-08-10';
     * nested exception is java.lang.IllegalArgumentException
     * @param date
     */
    @GetMapping("/date")
    public void date(Date date) {
        System.out.println(date);
    }
}
