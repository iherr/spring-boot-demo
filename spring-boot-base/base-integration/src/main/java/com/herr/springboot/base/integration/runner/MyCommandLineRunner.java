package com.herr.springboot.base.integration.runner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author herr05
 */
@Component
@Order(99)
public class MyCommandLineRunner implements CommandLineRunner {
    public static final Logger log = LoggerFactory.getLogger(MyApplicationRunner.class);

    @Override
    public void run(String... args) throws Exception {
        log.info("MyCommandLineRunner--->"+ Arrays.toString(args));
    }
}
