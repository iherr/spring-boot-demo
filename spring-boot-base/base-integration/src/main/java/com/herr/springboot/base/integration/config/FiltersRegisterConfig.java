package com.herr.springboot.base.integration.config;

/**
 * @author herr05
 */

import com.herr.springboot.base.integration.extend.CorsFilter;
import com.herr.springboot.base.integration.extend.LogFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 过滤器自注册
 */
@Configuration
public class FiltersRegisterConfig {
    @Bean
    public FilterRegistrationBean corsFilter(){
        FilterRegistrationBean corsFilterRegistrationBean = new FilterRegistrationBean();
        corsFilterRegistrationBean.setFilter(new CorsFilter());
        corsFilterRegistrationBean.addUrlPatterns("/*");
        corsFilterRegistrationBean.setName("corsFilter");
        corsFilterRegistrationBean.setOrder(2);
        return corsFilterRegistrationBean;
    }

    @Bean
    public FilterRegistrationBean logFilter(){
        FilterRegistrationBean logFilterRegistrationBean = new FilterRegistrationBean();
        logFilterRegistrationBean.setFilter(new LogFilter());
        logFilterRegistrationBean.addUrlPatterns("/*");
        logFilterRegistrationBean.setName("logFilter");
        logFilterRegistrationBean.setOrder(1);
        return logFilterRegistrationBean;
    }
}
