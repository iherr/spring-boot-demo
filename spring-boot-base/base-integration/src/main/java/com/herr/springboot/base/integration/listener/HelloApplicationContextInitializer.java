package com.herr.springboot.base.integration.listener;

import com.herr.springboot.base.integration.runner.MyApplicationRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;


public class HelloApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {


    public static final Logger log = LoggerFactory.getLogger(HelloApplicationContextInitializer.class);

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        log.info("ApplicationContextInitializer--->initialize--->"+configurableApplicationContext);
    }
}
