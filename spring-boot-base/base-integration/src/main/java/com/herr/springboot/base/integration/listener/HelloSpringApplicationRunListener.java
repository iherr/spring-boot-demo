package com.herr.springboot.base.integration.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;


public class HelloSpringApplicationRunListener implements SpringApplicationRunListener {

    public static final Logger log = LoggerFactory.getLogger(HelloSpringApplicationRunListener.class);

    public HelloSpringApplicationRunListener(SpringApplication application, String[] args) {

    }

//    @Override
//    public void starting() {
//        log.info("SpringApplicationRunListener--->startting--->");
//    }
//
//    @Override
//    public void environmentPrepared(ConfigurableEnvironment environment) {
//        Object o = environment.getSystemProperties().get("os.name");
//        log.info("SpringApplicationRunListener--->environmentPrepared--->"+o);
//
//    }

    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {
        log.info("SpringApplicationRunListener--->contextPrepared--->");

    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
        log.info("SpringApplicationRunListener--->contextLoaded--->");

    }

//    @Override
//    public void running(ConfigurableApplicationContext context) {
//        log.info("SpringApplicationRunListener--->running--->");
//
//    }
}
