package com.herr.springboot.base.integration.extend;

/**
 * 自定义业务异常
 * @author herr05
 */

public class BizException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private Integer code;
	private String message;
	private Object data;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public BizException(String message) {
		super(message);
		this.code=1000;
		this.message=message;
	}

	public BizException(Integer code){
		super();
		this.code=code;
	}

	public BizException(Integer code, String message) {
		super(message);
		this.code = code;
		this.message = message;
	}

	public BizException(Integer code, String message, Object data) {
		super(message);
		this.code = code;
		this.message = message;
		this.data = data;
	}
}
