package com.herr.springboot.grpc.hello.client;

import com.herr.springboot.grpc.client.GrpcClient;
import com.herr.springboot.grpc.hello.api.HelloRequest;
import com.herr.springboot.grpc.hello.api.HelloResponse;
import com.herr.springboot.grpc.hello.api.HelloServiceGrpc;
import io.grpc.ManagedChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.herr.springboot.grpc")
public class HelloClientApplication implements CommandLineRunner {

  @Autowired
  private GrpcClient grpcClient;

  @Override
  public void run(String... args) throws Exception {
    ManagedChannel channel = grpcClient.buildChannel();
    try {
      HelloServiceGrpc.HelloServiceBlockingStub helloService = HelloServiceGrpc.newBlockingStub(channel);
      HelloRequest request = HelloRequest
          .newBuilder()
          .setName("world")
          .build();
      HelloResponse response = helloService.say(request);
      System.out.println(response.getMessage());
    } finally {
      channel.shutdown();
    }
  }

  public static void main(String[] args) {
    SpringApplication.run(HelloClientApplication.class, args).close();
  }
}
