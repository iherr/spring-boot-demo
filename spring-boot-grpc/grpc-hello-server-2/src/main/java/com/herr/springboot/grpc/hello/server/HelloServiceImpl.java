package com.herr.springboot.grpc.hello.server;

import com.herr.springboot.grpc.hello.api.HelloRequest;
import com.herr.springboot.grpc.hello.api.HelloResponse;
import com.herr.springboot.grpc.hello.api.HelloServiceGrpc;
import com.herr.springboot.grpc.server.GrpcService;
import io.grpc.stub.StreamObserver;

@GrpcService
public class HelloServiceImpl extends HelloServiceGrpc.HelloServiceImplBase {

  @Override
  public void say(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
    HelloResponse response = null;
    try {
      response = HelloResponse
          .newBuilder()
          .setMessage("hello " + request.getName())
          .build();
    } catch (Exception e) {
      responseObserver.onError(e);
    } finally {
      responseObserver.onNext(response);
    }
    responseObserver.onCompleted();
  }
}