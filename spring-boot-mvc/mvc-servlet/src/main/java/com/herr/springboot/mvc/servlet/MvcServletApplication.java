package com.herr.springboot.mvc.servlet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan(basePackages = "com.herr.springboot.mvc.servlet")
public class MvcServletApplication {

    public static void main(String[] args) {
        SpringApplication.run(MvcServletApplication.class, args);
    }
}
