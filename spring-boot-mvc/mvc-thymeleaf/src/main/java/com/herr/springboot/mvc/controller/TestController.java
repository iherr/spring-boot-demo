package com.herr.springboot.mvc.controller;

import com.herr.springboot.mvc.entity.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author herr05
 */
@Controller
public class TestController {

        @RequestMapping("/index")
        public ModelAndView index(){
            ModelAndView mv = new ModelAndView();
            mv.addObject("name","hello herr");
            mv.setViewName("/index.html");
            Person p1 = new Person("iherr", 18, "male");
            Person p2 = new Person("zhangsan", 20, "famale");
            ArrayList<Person> list = new ArrayList<>();
            list.add(p1);
            list.add(p2);
            mv.addObject("plist",list);
            mv.addObject("person",p1);
            return mv;
        }
}
