<!DOCTYPE html>
<%--声明要引用的标签库--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- HTML注释，客户端可显示 -->
<%-- JSP注释，发送到服务端，客户端无法显示--%>
<html lang="en">

<body>

<%-- JSP声明语句用于声明变量和方法 --%>
<%!
    public int a=1;            //声明整型变量a
    public String print(){    //声明方法print
        return "JSP method";
    }
%>
<p>
    Message: ${message}
</p>


<%-- JJSPScriptlet是一段Java代码 --%>
<p>
    <%
        String name="herr";
        out.print("hello "+name);
    %>
</p>

<%-- JSP表达式 --%>
<p>
    <%=a%>
</p>
</body>

</html>