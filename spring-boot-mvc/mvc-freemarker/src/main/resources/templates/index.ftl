<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>FreeMarker</title>
</head>
<body>
    <#-- 注释不会输出到html -->
    <h1>hello world ftl</h1>
    <h1 style="color: red">${name}</h1> <#-- 插值 -->
    <h1 style="color: red">${sex!'男'}</h1> <#-- 如为null的默认值 -->

    <#assign age = 20 /> <#-- 定义变量 -->
    <#if age == 18>
        <p> age = 18</p>
    <#else>
        <p> age != 18</p>
    </#if>

    <#list list as item> <#-- List集合取值 -->
        ${item!}<br/>
    </#list>

    <#list map?keys as key> <#-- Map集合取值 -->
        ${key}:${map[key]}
    </#list>

    <br/>
    ${htmlstr?html} <#-- 转义HTML内容 -->

    <br/>
    ${htmlstr}  <#-- 不转义HTML内容，直接以html展示 -->
</body>
</html>