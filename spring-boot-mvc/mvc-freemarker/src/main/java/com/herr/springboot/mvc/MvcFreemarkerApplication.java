package com.herr.springboot.mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvcFreemarkerApplication {
    public static void main(String[] args) {
        SpringApplication.run(MvcFreemarkerApplication.class, args);
    }
}