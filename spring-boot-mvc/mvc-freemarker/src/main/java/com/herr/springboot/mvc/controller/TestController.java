package com.herr.springboot.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author herr05
 */
@Controller
public class TestController {

        @RequestMapping("/index")
        public String index(Model model){
            model.addAttribute("name","hello herr");

            List<String> list = new ArrayList<String>();
            list.add("第一个值");
            list.add("第二个值");
            list.add("第三个值");
            model.addAttribute("list",list);

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("name","herr");
            map.put("age",18);
            map.put("sex","male");
            model.addAttribute("map",map);

            model.addAttribute("htmlstr","<p style='color:red'>我只是个html</p>");

            return "index";
        }
}
