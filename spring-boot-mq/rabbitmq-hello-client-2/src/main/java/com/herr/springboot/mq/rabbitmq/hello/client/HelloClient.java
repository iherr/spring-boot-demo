package com.herr.springboot.mq.rabbitmq.hello.client;

import com.herr.springboot.mq.rabbitmq.rpc.client.RpcSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HelloClient {

  @Autowired
  private RpcSender<String, String> rpcSender;

  public String send(String message) {
    return rpcSender.send(message);
  }
}
