package com.herr.springboot.mq.rabbitmq.hello.server;

import com.herr.springboot.mq.rabbitmq.rpc.server.RpcReceiver;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class HelloServer implements RpcReceiver<String, String> {

  @Override
  @RabbitListener(queues = "rpc-queue")
  public String receive(String message) {
    return "hello " + message;
  }
}
