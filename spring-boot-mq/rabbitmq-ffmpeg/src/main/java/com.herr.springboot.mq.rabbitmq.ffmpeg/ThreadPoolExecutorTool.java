package com.herr.springboot.mq.rabbitmq.ffmpeg;

import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ThreadPoolExecutorTool {
    public ExecutorService getSingleThreadPoolExecutor() {
        return Executors.newFixedThreadPool(2);
    }
}
