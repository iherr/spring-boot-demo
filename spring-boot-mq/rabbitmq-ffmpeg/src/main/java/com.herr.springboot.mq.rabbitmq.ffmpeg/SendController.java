package com.herr.springboot.mq.rabbitmq.ffmpeg;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author herr05
 */
@RestController
public class SendController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RequestMapping("/send")
    public String send(){
        String str = UUID.randomUUID().toString();
        System.out.println("before send:" + LocalDateTime.now() +",str:"+ str);
        rabbitTemplate.convertAndSend("hello-queue", str);
        System.out.println("end send   :" + LocalDateTime.now() +",str:"+ str);

        //System.out.println("before send2:" + LocalDateTime.now() +",str:"+ str);
        //rabbitTemplate.convertAndSend("hello-queue2", str);
        //System.out.println("end send2:" + LocalDateTime.now() +",str:"+ str);
        return "send";
    }




}
