package com.herr.springboot.mq.rabbitmq.ffmpeg;

import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FFmpegApplication {

  @Bean
  public Queue helloQueue() {
    return new Queue("hello-queue");
  }

  @Bean
  public Queue helloQueue2() {
    return new Queue("hello-queue2");
  }

  public static void main(String[] args) {
    SpringApplication.run(FFmpegApplication.class, args);
  }
}
