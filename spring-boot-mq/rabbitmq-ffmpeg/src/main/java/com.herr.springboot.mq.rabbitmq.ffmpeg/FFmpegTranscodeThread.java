package com.herr.springboot.mq.rabbitmq.ffmpeg;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FFmpegTranscodeThread implements Runnable {

    private String msg;

	public FFmpegTranscodeThread(String msg) {
        this.msg = msg;
	}

    @Override
	public void run() {
        try {
            //Thread.sleep(5000);
            System.out.println("Thread:"+Thread.currentThread().getName()+"beign ffmpeg:" + LocalDateTime.now() +",msg:"+msg);

            List<String> commands = new ArrayList<>();
            commands.add("-i");
            commands.add("/home/appadmin/repo/DATA/private/upload/file/resource/1111111111111111111111111111111/05in.mp4");
            commands.add("-s");
            commands.add("1280x720");
            String s = UUID.randomUUID().toString();
            commands.add("/home/appadmin/repo/DATA/private/upload/file/resource/1111111111111111111111111111111/05out-"+s+".mp4");
            FFmpegUtil.executeCommand(commands);

            System.out.println("Thread:"+Thread.currentThread().getName()+"end ffmpeg:" + LocalDateTime.now() +",msg:"+msg);
        }catch (Exception e){
            e.printStackTrace();
        }

	}
}
