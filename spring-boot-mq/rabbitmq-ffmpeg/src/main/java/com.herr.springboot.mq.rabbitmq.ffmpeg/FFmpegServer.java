package com.herr.springboot.mq.rabbitmq.ffmpeg;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Component
public class FFmpegServer {

  ThreadPoolExecutor pool = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);

  @RabbitListener(queues = "hello-queue")
  public void receive(String message) throws InterruptedException {

    System.out.println("before receive:" + LocalDateTime.now() +",str:"+ message);
    //System.out.println(message);
    //Thread.sleep(5000);
    FFmpegTranscodeThread run = new FFmpegTranscodeThread(message);
    //run.run();

    //Thread thread = new Thread(run);
    //thread.start();

    //threadPoolExecutorTool.getSingleThreadPoolExecutor().execute(run);
    pool.execute(run);

    System.out.println("end receive   :" + LocalDateTime.now() +",str:"+ message);
  }

  @RabbitListener(queues = "hello-queue2")
  public void receive2(String message) throws InterruptedException {

    System.out.println("before receive2:" + LocalDateTime.now() +",str:"+ message);
    System.out.println(message);
    Thread.sleep(5000);
    System.out.println("end receive2:" + LocalDateTime.now() +",str:"+ message);
  }

}
