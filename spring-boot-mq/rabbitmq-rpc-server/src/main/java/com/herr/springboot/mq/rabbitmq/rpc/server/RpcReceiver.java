package com.herr.springboot.mq.rabbitmq.rpc.server;

public interface RpcReceiver<I, O> {

  O receive(I message);
}
